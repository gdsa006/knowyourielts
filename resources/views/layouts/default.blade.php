<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="no-cache">
<meta http-equiv="Expires" content="-1">
<meta http-equiv="Cache-Control" content="no-cache">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.1.0-4/css/all.css">

    
    <title>KnowYourIELTS - Everything you need</title>
</head>

<body>

@include('partials/navbar')
    <div class="page">
        @yield('content')
    </div>
@include('partials/footer')
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBu5nZKbeK-WHQ70oqOWo-_4VmwOwKP9YQ"></script>
    <script src="{{ asset('js/custom.js') }}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
    <script>
        var token = '{{ Session::token() }}';
        var urlCalculatorData = '{{ route('calcdata') }}';
        var urlSendMail = '{{ route('sendmail') }}';
        $('#result_loading_gif').hide();

    $('#calculate').on("click", function (e) {
  e.stopPropagation();
  $listening = $('.listeningVal').val();
  $reading = $('.readingVal').val();
  $writing = $('.writingVal').val();
  $speaking = $('.speakingVal').val();
  $(this).attr('disabled', 'disabled');
  $('#result_loading_gif').show();

  $.ajax({
      method: 'POST',
      url: urlCalculatorData,
      data: { listeningBand: $listening, readingBand: $reading, writingBand: $writing, speakingBand: $speaking, _token: token },
      success: function (data) {
        //   console.log(data.Data);
          $("#final-result").html("<span>Overall Scores: " + data.Data + "</span> &nbsp; <a href='https://www.britishcouncil.in/exam/ielts/book-test' style='font-size: 12px; text-decoration: underline; top:-2px; position: relative'>Book Your IELTS Test</a>");
          $('#result_loading_gif').hide();
          $('#calculate').removeAttr('disabled');
      }
  })
});
    </script>

    <script>
    var rangeSlider = function(){
  var slider = $('.range-slider'),
      range = $('.range-slider__range'),
      value = $('.range-slider__value');
    
  slider.each(function(){

    value.each(function(){
      var value = $(this).prev().attr('value');
      $(this).html(value);
    });

    range.on('input', function(){
      $(this).next(value).html(this.value);
    });
  });
};
rangeSlider();
    </script>

    <script>
      $('.navbar .dropdown').hover(function() {
    $(this).find('.dropdown-menu').first().stop(true, true).slideDown(150);
}, function() {
    $(this).find('.dropdown-menu').first().stop(true, true).slideUp(100);
});
      </script>

      <script>


// Sticky navbar
// =========================
if(window.matchMedia('(min-width: 768px)').matches)
    {
$(document).ready(function () {
                // Custom function which toggles between sticky class (is-sticky)
                var stickyToggle = function (sticky, stickyWrapper, scrollElement) {
                    var stickyHeight = sticky.outerHeight();
                    var stickyTop = stickyWrapper.offset().top;
                    if (scrollElement.scrollTop() >= stickyTop) {
                        stickyWrapper.height(stickyHeight);
                        sticky.addClass("is-sticky");
                    }
                    else {
                        sticky.removeClass("is-sticky");
                        stickyWrapper.height('auto');
                    }
                };

                // Find all data-toggle="sticky-onscroll" elements
                $('[data-toggle="sticky-onscroll"]').each(function () {
                    var sticky = $(this);
                    var stickyWrapper = $('<div>').addClass('sticky-wrapper'); // insert hidden element to maintain actual top offset on page
                    sticky.before(stickyWrapper);
                    sticky.addClass('sticky');

                    // Scroll & resize events
                    $(window).on('scroll.sticky-onscroll resize.sticky-onscroll', function () {
                        stickyToggle(sticky, stickyWrapper, $(this));
                    });

                    // On page load
                    stickyToggle(sticky, stickyWrapper, $(window));
                });
            });
          }
        </script>
<script>
	function count(){
	  var txtVal = $('.wordCounter').val();
	  var words = txtVal.trim().replace(/\s+/gi, ' ').split(' ').length;
	  var chars = txtVal.length;
    var fullStopCount =  ($('.wordCounter').val().match(/\./g) || []).length;
    if(chars===0){words=0;}
    if((chars === 0)){
      paras = 0;
    }
    if((chars === 1)){
      paras = 1;
    }
    if((chars >= 1)){
      var paras = ($('.wordCounter').val().match(/[\n\r]/g) || []).length + 1;
    }
	  $('#counter').html('<br>'+words+' words and '+ chars +' characters and ' + fullStopCount + ' sentences and ' + paras + ' paragraphs' );
	}
  count();
  
$('textarea').on('keyup propertychange paste', function(){ 
    count();
});
</script>
<script>
$('#sending-contact-email').hide();
$('.confirmMail').hide();
$('.sendemail').on('submit', function(e){
    e.preventDefault();
    var form = document.forms.namedItem("sendemail");
   
    $.ajax({
        method: 'POST',
        url: urlSendMail,
        data: new FormData(form),
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
          console.log(data.message);
          if(data.message.name != undefined){
            $('#name-error').html(data.message.name);
          }
          else{
            $('#name-error').html('');
          }
          if(data.message.email  != undefined){
            $('#email-error').html(data.message.email);
          }
          else{
            $('#email-error').html('');
          }
          if(data.message.message  != undefined){
            $('#message-error').html(data.message.message);
          }
          else{
            $('#message-error').html('');
          }
          if((data.message.name == undefined) && (data.message.email == undefined) && (data.message.message == undefined)){
            $('#sending-contact-email').show();
            $('#send-contact-email').hide();
          }
          if(data.message == 'success'){
            $('#sending-contact-email').hide();
            $('#send-contact-email').show();
            $('.cInput').val(null);
            setTimeout(function() {
              $('.confirmMail').show('slow').delay(2000).hide('slow');
            }, 1000); // <-- time in milliseconds
          }
        }
    })
}); 
</script>
<script>
  $(document).ready(function() {
    $(".alert-info").fadeTo(5000, 500).slideUp(500, function() {
      $(".alert-info").slideUp(500);
    });
  });
  </script>
</body>

</html>