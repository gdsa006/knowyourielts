@extends('layouts/default')
@section('content')
<nav aria-label="breadcrumb" class=" breadcrumb-bg">
    <div class="container">
        <ol class="breadcrumb rounded-0 m-0">
            <li class="breadcrumb-item "><a href="#" class="">Home</a></li>
            <li class="breadcrumb-item "><a href="#" class="">Ebooks</a></li>
        </ol>
    </div>
</nav>
<!-- <div class="page-title-bar">

</div> -->


<section>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="">
                    <h5 class="mb-3">Word Counter</h5>
                    <p>
                        You can type your characters and words into the text area. The counter will be updated instantly, displaying the amount of characters, words, sentences, and paragraphs in your text.
                    </p>
                </div>
                <div class="form-group">
                    <label for="sel1">
                        <div id="counter"></div>
                    </label>
                    <textarea rows="16" style="resize: none" class="form-control wordCounter rounded-0"></textarea>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card my-4">
                    <div class="card-body">
                        <img src="/images/300x600_ad.jpg" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>








@stop