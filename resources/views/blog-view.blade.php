@extends('layouts/default')
@section('content')
<nav aria-label="breadcrumb" class="breadcrumb-bg">
    <div class="container">
    <ol class="breadcrumb rounded-0 m-0">
        <li class="breadcrumb-item "><a href="/">Home</a></li>
        <li class="breadcrumb-item "><a href="/blog/">Blog</a></li>
        <li class="breadcrumb-item "><a href="/blog/{{$blog->slug}}">{{$blog->title}}</a></li>
    </ol>
    </div>
</nav>
<section class="py-2">
<div class="container">
    <div class="row">
    <div class="col-lg-8">

<!-- Title -->
<h1 class="mt-4">{{$blog->title}}</h1>

<!-- Author -->
<p class="lead">
  by
  <a href="#">{{$blog->author}}</a>
</p>

<hr>

<!-- Date/Time -->
<p>Posted on {{Carbon\Carbon::parse($blog->created_at)->format('dS F Y')}}</p>

<hr>

<!-- Preview Image -->
<img class="img-fluid rounded" src="/images/admin_uploads/{{$blog->image}}" alt="{{$blog->image}}" style="object-fit: cover; height: 400px; width: 100%;">

<hr>

<!-- Post Content -->
{!! $blog->description !!}
<div class="card my-4">
          <div class="card-body">
          <img src="/images/728x90_ad.jpg" class="img-fluid">
          </div>
        </div>
</div>


<div class="col-md-4 newsevents-sidebar">

        <!-- Search Widget -->
        <div class="card my-4 d-none">
          <h5 class="card-header">Search</h5>
          <div class="card-body">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Search for...">
              <span class="input-group-btn">
                <button class="btn btn-secondary" type="button">Go!</button>
              </span>
            </div>
          </div>
        </div>

        <!-- Categories Widget -->
        <div class="card my-4 d-none">
          <h5 class="card-header">Categories</h5>
          <div class="card-body">
            <div class="row">
              <div class="col-lg-6">
                <ul class="list-unstyled mb-0">
                  <li>
                    <a href="#">Web Design</a>
                  </li>
                  <li>
                    <a href="#">HTML</a>
                  </li>
                  <li>
                    <a href="#">Freebies</a>
                  </li>
                </ul>
              </div>
              <div class="col-lg-6">
                <ul class="list-unstyled mb-0">
                  <li>
                    <a href="#">JavaScript</a>
                  </li>
                  <li>
                    <a href="#">CSS</a>
                  </li>
                  <li>
                    <a href="#">Tutorials</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>

        <!-- Side Widget -->
        <div class="card my-4">
          <div class="card-body">
          <img src="/images/300x600_ad.jpg" class="img-fluid">
          </div>
        </div>

        <div class="card my-4">
          <div class="card-body">
          <img src="/images/300x250_ad.jpg" class="img-fluid">
          </div>
        </div>

      </div>


    </div>
</div>
</section>




@stop