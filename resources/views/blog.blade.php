@extends('layouts/default')
@section('content')

<nav aria-label="breadcrumb" class="breadcrumb-bg">
    <div class="container">
        <ol class="breadcrumb rounded-0 m-0">
            <li class="breadcrumb-item "><a href="/">Home</a></li>
            <li class="breadcrumb-item "><a href="/blog/">Blog</a></li>
        </ol>
    </div>
</nav>

<section class="pb-2 pt-4" style="background-color: #f5f5f5;"> 
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <img src="/images/top-ad.png" class="img-fluid">
            </div>
        </div>
</section>

<section class="p-4 pt-0" style="background-color: #f5f5f5;">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="row">


                    @php
                    $arr = array();
                    for($i=1;$i<=count($blogs);$i++){ $arr[]=(3 * $i - 2); } @endphp @foreach($blogs as $blog) @php $val=$loop->iteration @endphp
                        <div class="@if(in_array($val, $arr)) col-lg-12 col-sm-12 wide @else col-lg-6 col-sm-12 grid @endif newsevents-item">
                            <div class="card h-100">
                                <a href="/blog/{{$blog->slug}}"><img class="card-img-top" src="images/admin_uploads/{{$blog->image}}" alt=""></a>
                                <div class="card-body d-flex flex-column">
                                    <a href="/blog/{{$blog->slug}}">
                                        <h4 class="card-title">{{$blog->title}}</h4>
                                    </a>
                                    <ul class="list-unstyled list-inline info">
                                        <li class="list-inline-item">
                                            <span><i class="fas fa-newspaper"></i></span> {{Carbon\Carbon::parse($blog->created_at)->format('dS F Y')}}
                                        </li>
                                        <li class="list-inline-item">
                                            <span><i class="fas fa-user"></i></span> {{$blog->author}}
                                        </li>
                                        <!-- <li class="list-inline-item">
                                            <span>Updated:</span> {{$blog->updated_at->diffForHumans()}}
                                        </li> -->
                                    </ul>
                                    </ul>
                                    {!! str_limit($blog->description, $limit = 125, $end = '...') !!}
                                </div>
                            </div>
                        </div>

                        @endforeach
                </div>
                <div>{{$blogs->render()}}</div>
            </div>
            <div class="col-md-4 newsevents-sidebar">
                <!-- Search Widget -->
                <div class="card d-none">
                    <h5 class="card-header">Search</h5>
                    <div class="card-body">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
                                <button class="btn btn-secondary" type="button">Go!</button>
                            </span>
                        </div>
                    </div>
                </div>

                <!-- Categories Widget -->
                <div class="card my-4 d-none">
                    <h5 class="card-header">Categories</h5>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <ul class="list-unstyled mb-0">
                                    <li>
                                        <a href="#">Web Design</a>
                                    </li>
                                    <li>
                                        <a href="#">HTML</a>
                                    </li>
                                    <li>
                                        <a href="#">Freebies</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-6">
                                <ul class="list-unstyled mb-0">
                                    <li>
                                        <a href="#">JavaScript</a>
                                    </li>
                                    <li>
                                        <a href="#">CSS</a>
                                    </li>
                                    <li>
                                        <a href="#">Tutorials</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Side Widget -->
                <div class="card my-4">
                    <h5 class="card-header">Side Widget</h5>
                    <div class="card-body">
                        You can put anything you want inside of these side widgets.
                    </div>
                </div>


                <div class="card my-4">
                    <div class="card-body">
                        <img src="/images/300x600_ad.jpg" class="img-fluid">
                    </div>
                </div>


            </div>
        </div>
    </div>
</section>
@stop