@extends('layouts/default')
@section('content')
<nav aria-label="breadcrumb" class="breadcrumb-bg">
    <div class="container">
        <ol class="breadcrumb rounded-0 m-0">
            <li class="breadcrumb-item "><a href="/">Home</a></li>
            <li class="breadcrumb-item "><a href="{{route('ieltsSpeaking')}}">Speaking</a></li>
            <li class="breadcrumb-item "><a href="{{route('speakingCueCards')}}">Cue Cards</a></li>
            <li class="breadcrumb-item "><a href="{{route('speakingCueCards')}}">#{{$cuecard_find->id}}</a></li>
        </ol>
    </div>
</nav>

<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3 class="mb-3 w-100">{{$cuecard_find->title}} - #{{$cuecard_find->id}}</h3>
                <div class="pb-3">
                    <h6>You should say:</h6>
                    {!! $cuecard_find->say !!}
                </div>
                <div class="pb-3">
                    <h6>and explain how you felt about this crowded place.</h6>
                    {!! $cuecard_find->explain !!}
                </div>
                <div class="pb-3">
                    @foreach($cuecard_models as $key=>$cuecard)
                        <h6>Model {{$key + 1}}</h6>
                        {!! $cuecard->model !!}
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>




@stop