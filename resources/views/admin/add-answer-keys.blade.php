@extends('admin/layouts/default')
@section('admin-content')


        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Add Answer Key</h1>
          <p class="mb-4"><a href="{{route('adminBlogs')}}"><i class="fas fa-arrow-left"></i> back to view</a></p>
          {{ Form::open(array('url' => 'admin/answer-keys/store', 'class' => '' )) }}
          <div class="row row-eq-height mb-4">
          <!-- DataTales Example -->
          <div class="col-8">
          <div class="card shadow mb-4 h-100">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              <h6 class="m-0 font-weight-bold text-primary">Add Details</h6>
              <div class="dropdown no-arrow d-none">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
            </div>
            <div class="card-body">
                    <div class="form-group">
                        <label>Answer Keys</label>
                      <textarea name="answerkeys" class="form-control answerkeys-answerkeys summernote"></textarea>
                      <small id="answerkeys" class="form-text text-danger">{{ $errors->first('answerkeys') }}</small>
                    </div>     
            </div>
          </div>
          </div>
          <div class="col-4">
          <div class="card shadow mb-4 h-100">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              <h6 class="m-0 font-weight-bold text-primary">More Details</h6>
              <div class="dropdown no-arrow d-none">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
            </div>
            <div class="card-body">
            <div class="form-group text-center">
<input @if(Session::has('alert-success')) type="button" value="Published" disabled @else type="submit" value="Submit" @endif class="btn btn-primary btn-block" value="Submit">
                    </div>
                    <hr>
                    <input id="file_name" type="hidden" value="default.jpg" name="image">
                    <div class="form-group">
                        <label>Course Category</label>
                      <select class="form-control" name="category">
                          <option selected>Speaking</option>
                          <option>Reading</option>
                          <option>Listening</option>
                          <option>Writing</option>
                      </select>
                      <small id="author" class="form-text text-danger">{{ $errors->first('category') }}</small>
                    </div>
                    <div class="form-group">
                        <label>Test Type</label>
                        <select class="form-control" name="test">
                          <option selected>General</option>
                          <option>Academic</option>
                      </select>
                      <small id="test" class="form-text text-danger">{{ $errors->first('test') }}</small>
                    </div>
                    <div class="form-group">
                        <label>Exam Date</label>
                      <input type="date" name="exam" class="form-control cuecard-exam" id="exampleInputKeywords" aria-describedby="emailHelp" placeholder="">
                      <small id="exam" class="form-text text-danger">{{ $errors->first('exam') }}</small>
                    </div>
            </div>
          </div>
          </div>
          </div>



          {{ Form::close() }}

        </div>
        <!-- /.container-fluid -->






@stop