@extends('admin/layouts/default')
@section('admin-content')


        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Add Blog Post</h1>
          <p class="mb-4"><a href="{{route('adminBlogs')}}"><i class="fas fa-arrow-left"></i> back to view</a></p>
          {{ Form::open(array('url' => 'admin/blogs/store', 'class' => '' )) }}
          <div class="row row-eq-height mb-4">
          <!-- DataTales Example -->
          <div class="col-8">
          <div class="card shadow mb-4 h-100">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              <h6 class="m-0 font-weight-bold text-primary">Add Details</h6>
              <div class="dropdown no-arrow d-none">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
            </div>
            <div class="card-body">
            
                    <div class="form-group">
                        <label>Enter Title</label>
                      <input type="text" name="title" class="form-control cuecard-title" id="exampleInputTitle" aria-describedby="emailHelp" placeholder="">
                      <small id="title" class="form-text text-danger">{{ $errors->first('title') }}</small>
                    </div>
                    <div class="form-group">
                        <label>Slug</label>
                      <input type="text" name="slug" class="form-control cuecard-slug" id="exampleInputSlug" aria-describedby="emailHelp" placeholder="">
                      <small id="slug" class="form-text text-danger">{{ $errors->first('slug') }}</small>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                      <textarea name="description" class="form-control summernote"></textarea>
                      <small id="description" class="form-text text-danger">{{ $errors->first('description') }}</small>
                    </div>     
            </div>
          </div>
          </div>
          <div class="col-4">
          <div class="card shadow mb-4 h-100">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              <h6 class="m-0 font-weight-bold text-primary">More Details</h6>
              <div class="dropdown no-arrow d-none">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
            </div>
            <div class="card-body">
            <div class="form-group text-center">
<input @if(Session::has('alert-success')) type="button" value="Published" disabled @else type="submit" value="Submit" @endif class="btn btn-primary btn-block" value="Submit">
                    </div>
                    <hr>
                    <input id="file_name" type="hidden" value="default.jpg" name="image">
                    <div class="form-group">
                        <label>Author</label>
                      <input type="text" name="author" class="form-control cuecard-title" id="exampleInputAuthor" aria-describedby="emailHelp" placeholder="">
                      <small id="author" class="form-text text-danger">{{ $errors->first('author') }}</small>
                    </div>
                    <div class="form-group">
                        <label>Tags</label>
                      <input type="text" name="tags" class="form-control cuecard-tags" id="exampleInputTags" aria-describedby="emailHelp" placeholder="">
                      <small id="tags" class="form-text text-danger">{{ $errors->first('tags') }}</small>
                    </div>
                    <div class="form-group">
                        <label>Keywords</label>
                      <input type="text" name="keywords" class="form-control cuecard-keywords" id="exampleInputKeywords" aria-describedby="emailHelp" placeholder="">
                      <small id="tags" class="form-text text-danger">{{ $errors->first('keywords') }}</small>
                    </div>
            </div>
          </div>
          </div>
          </div>



          {{ Form::close() }}

<div class="row">
          <div class="col-12">
          <div class="card shadow mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              <h6 class="m-0 font-weight-bold text-primary">Picture</h6>
              <div class="dropdown no-arrow d-none">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
            </div>
            <div class="card-body text-center">
            @include('admin/partials/image-uploader')
            </div>
          </div>
          </div>



          </div>

        </div>
        <!-- /.container-fluid -->






@stop