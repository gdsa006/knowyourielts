@extends('admin/layouts/default')
@section('admin-content')


        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Answer Keys</h1>
          <p class="mb-4">Descriptive Lines</a>.</p>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              <h6 class="m-0 font-weight-bold text-primary">Answer Keys</h6>
              <a href="{{route('adminAnswerKeysAdd')}}" class="btn btn-primary btn-circle btn-sm">
                    <i class="fas fa-plus"></i>
                  </a>
              <div class="dropdown no-arrow d-none">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered answer-keys" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Category</th>
                      <th>Test</th>
                      <th>Exam</th>
                      <th>Created</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>ID</th>
                      <th>Category</th>
                      <th>Test</th>
                      <th>Exam</th>
                      <th>Created</th>
                      <th>Status</th>
                    </tr>
                  </tfoot>
                  <tbody>
                      @foreach($answerkeys as $answerkey)
                    <tr>
                      <td><a href="/admin/answer-keys/edit/{{$answerkey->id}}">{{$answerkey->id}}</a></td>
                      <td>{{$answerkey->category}}</td>
                      <td>{{$answerkey->test}}</td>
                      <td>{{$answerkey->exam}}</td>
                      <td>{{Carbon\Carbon::parse($answerkey->created_at)->format('dS F Y')}}</td>
                      <td align=""><span style="cursor: pointer" role="button" id="Answerkey-status-button-{{$answerkey->id}}" @if($answerkey->status) data-poststatus = "{{$answerkey->status}}" @endif data-posttype = "Answerkey" data-id = "{{$answerkey->id}}" class="font-weight-bold @if($answerkey->status) text-secondary @else text-success @endif text-white post_status">@if($answerkey->status) Unpublish @else Publish @endif</span></td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

        @if(Session::has('alert-success')) 
            <style>
                .cuecards-table tbody tr:first-child *{
                  color: #000000 !important
                }
            </style>

        @endif




@stop