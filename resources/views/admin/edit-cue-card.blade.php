@extends('admin/layouts/default')
@section('admin-content')
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Edit #{{$cuecard->id}} Cue Card</h1>
          <p class="mb-4"><a href="{{route('adminSpeakingCueCards')}}"><i class="fas fa-arrow-left"></i> back to view</a></p>
          
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              <h6 class="m-0 font-weight-bold text-primary">Edit</h6>
              <div class="dropdown no-arrow d-none">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
            </div>
            <div class="card-body">
            {{ Form::open(array('url' => 'admin/speaking/cue-cards/update/'.$cuecard->id, 'class' => '' )) }}
                    <div class="form-group">
                        <label>Enter Title</label>
                      <input type="text" name="title" value="{{$cuecard->title}}" class="form-control cuecard-title" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="">
                      <small id="title" class="form-text text-danger">{{ $errors->first('title') }}</small>
                    </div>
                    <div class="form-group">
                        <label>What to say</label>
                      <textarea name="what_to_say" class="form-control summernote" rows="5">{{$cuecard->say}}</textarea>
                      <small id="title" class="form-text text-danger">{{ $errors->first('what_to_say') }}</small>
                    </div>
                    <div class="form-group">
                        <label>Explain what</label>
                      <textarea name="explain_what" class="form-control summernote" rows="3" name="editordata">{{$cuecard->explain}}</textarea>
                      <small id="title" class="form-text text-danger">{{ $errors->first('explain_what') }}</small>
                    </div>
                    <div id="model">
                    @foreach($cuecard->cuecardmodels as $i=>$model )
                    <div class="form-group" id="model1">
                        <label id="model-label1">Model {{$i + 1}}</label>
                      <textarea name="model[]" class="form-control summernote" rows="8" name="editordata">{{$model->model}}</textarea>
                    </div>
                    @endforeach
                    </div>
                    <div class="form-group d-none">
                    <a href="#" id="add-more" name="add-more" class="btn btn-light btn-icon-split">
                    <span class="icon text-gray-600">
                      <i class="fas fa-arrow-right"></i>
                    </span>
                    <span class="text">Add Model</span>
                  </a>
                    </div>
                    <hr>
                    <div class="form-group text-center">
                        <input @if(Session::has('alert-success')) type="button" value="Updated" disabled @else type="submit" value="Update" @endif class="btn btn-primary" value="Submit">
                    </div>
                    {{ Form::close() }}
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->
@stop