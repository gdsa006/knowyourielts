<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>KYI AdminBoard</title>

  <!-- Custom fonts for this template-->
  <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link rel="stylesheet" type="text/css" href="{{ asset('css/admin-styles.css') }}">
  <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote-bs4.min.css" rel="stylesheet">
  <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
</head>

<body id="page-top">
<!-- Page Wrapper -->
<div id="wrapper">
@include('admin/partials/sidebar')
<div id="content-wrapper" class="d-flex flex-column">
<div id="content">
@include('admin/partials/navbar')
        @yield('admin-content')
        </div>
       
@include('admin/partials/footer')

</div>
        </div>



<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="{{route('logout')}}">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
  <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
  <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote-bs4.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{ asset('js/sb-admin-2.js') }}"></script>

  <!-- Page level plugins -->
  <script src="{{ asset('vendor/chart.js/Chart.min.js') }}"></script>
  <script src="{{ asset('js/demo/datatables-demo.js') }}"></script>
  <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

  <!-- Page level custom scripts -->

  @stack('chartarea')

  <!-- <script src="{{ asset('js/demo/chart-area-demo.js') }}"></script> -->
  <!-- <script src="{{ asset('js/demo/chart-pie-demo.js') }}"></script> -->

  <script>
      $('.summernote').summernote({
        placeholder: '',
        tabsize: 1,
        height: 100,
        toolbar: [
          // [groupName, [list of button]]
          ['para', ['ul', 'ol', 'paragraph']],
        ],
      });
    </script>

<script>
          var token = '{{ Session::token() }}';
        var adminUploadPic = "{{ route('adminUploadPic') }}";
        var updatePostStatus = '{{ route('updatePostStatus') }}';
    </script>
   
   <script>
$('.post_status').on("click", function (e) {
  e.stopPropagation();
  $posttype = $(this).data('posttype');
  $postid = $(this).data('id');
  $poststatus = $(this).data('poststatus');
  $('#result_loading_gif').show();

  $.ajax({
      method: 'POST',
      url: updatePostStatus,
      data: { posttype: $posttype, postid: $postid, poststatus: $poststatus, _token: token },
      success: function (data) {
        console.log(data.message);
        if(data.message == 'updated')
        {
          console.log(data);
          $unique_button_id = "#"+data.post_type+"-status-button-" + data.post_id;
          $($unique_button_id).data("poststatus", data.post_status);
          if(data.post_status == '0'){
            $($unique_button_id).removeClass("text-info");
            $($unique_button_id).addClass("text-success");
            $($unique_button_id).text("Publish");
          }
          else{
            $($unique_button_id).removeClass("text-success");
            $($unique_button_id).addClass("text-secondary");
            $($unique_button_id).text("Unpublish");
          }
        }
      }
  })
});
</script>

</body>

</html>
