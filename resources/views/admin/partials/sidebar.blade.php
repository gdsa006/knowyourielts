    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink d-none"></i>
        </div>
        <div class="sidebar-brand-text mx-3">KYI AdminBoard</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item {{ (Request::is('*dashboard') ? 'active' : '') }}">
        <a class="nav-link" href="{{route('adminDashboard')}}">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        IELTS Course
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item {{ (Request::is('*admin/speaking*') ? 'active' : '') }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>Speaking</span>
        </a>
        <div id="collapseOne" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Interactive Elements:</h6>
            <a class="collapse-item {{ (Request::is('*admin/speaking/cue-cards*') ? 'active' : '') }}" href="{{route('adminSpeakingCueCards')}}">Cue Cards</a>
            <a class="collapse-item" href="cards.html">Native Speaker</a>
          </div>
        </div>
      </li>

      <li class="nav-item {{ (Request::is('*admin/reading*') ? 'active' : '') }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>Reading</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Interactive Elements:</h6>
            <a class="collapse-item {{ (Request::is('*admin/reading/cue-cards*') ? 'active' : '') }}" href="{{route('adminSpeakingCueCards')}}">Cue Cards</a>
            <a class="collapse-item" href="cards.html">Native Speaker</a>
          </div>
        </div>
      </li>

      <li class="nav-item {{ (Request::is('*admin/listening*') ? 'active' : '') }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>Listening</span>
        </a>
        <div id="collapseThree" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Interactive Elements:</h6>
            <a class="collapse-item {{ (Request::is('*admin/listening/cue-cards*') ? 'active' : '') }}" href="{{route('adminSpeakingCueCards')}}">Cue Cards</a>
            <a class="collapse-item" href="cards.html">Native Speaker</a>
          </div>
        </div>
      </li>

      <li class="nav-item {{ (Request::is('*admin/writing*') ? 'active' : '') }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>Writing</span>
        </a>
        <div id="collapseFour" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Interactive Elements:</h6>
            <a class="collapse-item {{ (Request::is('*admin/writing/cue-cards*') ? 'active' : '') }}" href="{{route('adminSpeakingCueCards')}}">Cue Cards</a>
            <a class="collapse-item" href="cards.html">Native Speaker</a>
          </div>
        </div>
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item d-none">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-wrench"></i>
          <span>Utilities</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Custom Utilities:</h6>
            <a class="collapse-item" href="utilities-color.html">Colors</a>
            <a class="collapse-item" href="utilities-border.html">Borders</a>
            <a class="collapse-item" href="utilities-animation.html">Animations</a>
            <a class="collapse-item" href="utilities-other.html">Other</a>
          </div>
        </div>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        WEBSITE
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item {{ (Request::is('*admin/blogs*') ? 'active' : '') }} {{ (Request::is('*admin/news-events*') ? 'active' : '') }} {{ (Request::is('*admin/answer-keys*') ? 'active' : '') }} ">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>Posts</span>
        </a>
        <div id="collapseFive" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Login Screens:</h6>
            <a class="collapse-item {{ (Request::is('*admin/blogs*') ? 'active' : '') }}" href="{{route('adminBlogs')}}">Blogs</a>
            <a class="collapse-item {{ (Request::is('*admin/news-events*') ? 'active' : '') }}" href="{{route('adminNewsEvents')}}">News & Events</a>
            <div class="collapse-divider"></div>
            <h6 class="collapse-header">Other Pages:</h6>
            <a class="collapse-item {{ (Request::is('*admin/answer-keys*') ? 'active' : '') }}" href="{{route('adminAnswerKeys')}}">Answer Keys</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSix" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>Others</span>
        </a>
        <div id="collapseSix" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="404.html">Useful Links</a>
            <a class="collapse-item" href="blank.html">Personal Experiences</a>
            <a class="collapse-item" href="blank.html">Document Downloads</a>
          </div>
        </div>
      </li>

      <!-- Nav Item - Charts -->
      <li class="nav-item d-none">
        <a class="nav-link" href="charts.html">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Charts</span></a>
      </li>

      <!-- Nav Item - Tables -->
      <li class="nav-item d-none">
        <a class="nav-link" href="tables.html">
          <i class="fas fa-fw fa-table"></i>
          <span>Tables</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->