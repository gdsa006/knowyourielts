@if(isset($pass_blogImage))
<div class="form-group">
    {{ Form::open(array('url' => 'admin/uploadpic', 'class' => 'uploadPicForm', 'files' => 'true', 'enctype' => 'multipart/form-data', 'name' => 'uploadPicForm' )) }}
    
    <div class="thumbnail mb-3">
        <img src="/images/admin_uploads/{{$pass_blogImage}}" style="object-fit: cover; max-width: 500px; max-height: 300px;">
    </div>
    
    <span class="btn btn-light btn-block btn-file py-4" style="border-style: dashed; border-width: 2px; border-color: #ebecf0; cursor: pointer">
    Change Image <input type="file" name="file" class="uploadPic">
</span>
<small id="file" class="form-text text-danger text-center"></small>
<small id="" class="form-text text-dark text-center">Max Size: 2mb, allowed filetypes jpeg,png,jpg,gif</small>
</div>
{{ Form::close() }}

@else

<div class="form-group">
    {{ Form::open(array('url' => 'admin/uploadpic', 'class' => 'uploadPicForm', 'files' => 'true', 'enctype' => 'multipart/form-data', 'name' => 'uploadPicForm' )) }}
    
    
    <div class="thumbnail mb-3" style="display: none">
        <img src="" style="object-fit: cover; max-width: 500px; max-height: 300px;">
    </div>
    
    <span class="btn btn-light btn-block btn-file py-4" style="border-style: dashed; border-width: 2px; border-color: #ebecf0; cursor: pointer">
    Drag Image File/Upload <input type="file" name="file" class="uploadPic">
</span>
<small id="file" class="form-text text-danger text-center"></small>
<small id="" class="form-text text-dark text-center">Max Size: 2mb, allowed filetypes jpeg,png,jpg,gif</small>
</div>
{{ Form::close() }}

@endif