@extends('admin/layouts/default')
@section('admin-content')


        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Add News Events Post</h1>
          <p class="mb-4"><a href="{{route('adminNewsEvents')}}"><i class="fas fa-arrow-left"></i> back to view</a></p>
          {{ Form::open(array('url' => 'admin/news-events/store', 'class' => '' )) }}
          <div class="row row-eq-height mb-4">
          <!-- DataTales Example -->
          <div class="col-8">
          <div class="card shadow mb-4 h-100">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              <h6 class="m-0 font-weight-bold text-primary">Add Details</h6>
              <div class="dropdown no-arrow d-none">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
            </div>
            <div class="card-body">
            
                    <div class="form-group">
                        <label>Enter Title</label>
                      <input type="text" name="title" class="form-control cuecard-title" id="exampleInputTitle" aria-describedby="emailHelp" placeholder="">
                      <small id="title" class="form-text text-danger">{{ $errors->first('title') }}</small>
                    </div>
                    <div class="form-group">
                        <label>Slug</label>
                      <input type="text" name="slug" class="form-control cuecard-slug" id="exampleInputSlug" aria-describedby="emailHelp" placeholder="">
                      <small id="slug" class="form-text text-danger">{{ $errors->first('slug') }}</small>
                    </div>
                    <div class="form-group">
                        <label>Story</label>
                      <textarea name="story" class="form-control summernote"></textarea>
                      <small id="story" class="form-text text-danger">{{ $errors->first('story') }}</small>
                    </div>     
            </div>
          </div>
          </div>
          <div class="col-4">
          <div class="card shadow mb-4 h-100">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              <h6 class="m-0 font-weight-bold text-primary">More Details</h6>
              <div class="dropdown no-arrow d-none">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
            </div>
            <div class="card-body">
            <div class="form-group text-center">
<input @if(Session::has('alert-success')) type="button" value="Published" disabled @else type="submit" value="Submit" @endif class="btn btn-primary btn-block" value="Submit">
                    </div>
                    <hr>
                    <input id="file_name" type="hidden" value="default.jpg" name="image">
                    <div class="form-group">
                        <label>Author</label>
                      <input type="text" name="author" class="form-control newsevents-author" id="exampleInputAuthor" aria-describedby="emailHelp" placeholder="">
                      <small id="author" class="form-text text-danger">{{ $errors->first('author') }}</small>
                    </div>
                    <div class="form-group">
                        <label>Topic</label>
                      <input type="text" name="topic" class="form-control newsevents-tags" id="exampleInputTags" aria-describedby="emailHelp" placeholder="">
                      <small id="topic" class="form-text text-danger">{{ $errors->first('topic') }}</small>
                    </div>
                    <div class="form-group">
                        <label>Type</label>
                      <input type="text" name="type" class="form-control newsevents-type" id="exampleInputKeywords" aria-describedby="emailHelp" placeholder="">
                      <small id="tags" class="form-text text-danger">{{ $errors->first('type') }}</small>
                    </div>
            </div>
          </div>
          </div>
          </div>



          {{ Form::close() }}

<div class="row">
          <div class="col-12">
          <div class="card shadow mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              <h6 class="m-0 font-weight-bold text-primary">Picture</h6>
              <div class="dropdown no-arrow d-none">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
            </div>
            <div class="card-body text-center">
            @include('admin/partials/image-uploader')
            </div>
          </div>
          </div>



          </div>

        </div>
        <!-- /.container-fluid -->






@stop