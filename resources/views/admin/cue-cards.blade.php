@extends('admin/layouts/default')
@section('admin-content')


        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Cue Cards</h1>
          <p class="mb-4">Descriptive Lines</a>.</p>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              <h6 class="m-0 font-weight-bold text-primary">Cue Cards</h6>
              <a href="{{route('adminSpeakingCueCardsAdd')}}" class="btn btn-primary btn-circle btn-sm">
                    <i class="fas fa-plus"></i>
                  </a>
              <div class="dropdown no-arrow d-none">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered cuecards-table" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Title</th>
                      <th>Say</th>
                      <th>Explain</th>
                      <th>Created</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>ID</th>
                      <th>Title</th>
                      <th>Say</th>
                      <th>Explain</th>
                      <th>Created</th>
                      <th>Status</th>
                    </tr>
                  </tfoot>
                  <tbody>
                      @foreach($cuecards as $cuecard)
                    <tr>
                      <td><a href="/admin/speaking/cue-cards/edit/{{$cuecard->id}}">{{$cuecard->id}}</a></td>
                      <td>{{str_limit($cuecard->title, $limit = 40, $end = '...')}}</td>
                      <td>{{str_limit($cuecard->say, $limit = 30, $end = '...')}}</td>
                      <td>{{str_limit($cuecard->explain, $limit = 30, $end = '...')}}</td>
                      <td>{{Carbon\Carbon::parse($cuecard->created_at)->format('dS F Y')}}</td>
                      <td align=""><span style="cursor: pointer" role="button" id="Cuecard-status-button-{{$cuecard->id}}" @if($cuecard->status) data-poststatus = "{{$cuecard->status}}" @endif data-posttype = "Cuecard" data-id = "{{$cuecard->id}}" class="font-weight-bold @if($cuecard->status) text-secondary @else text-success @endif text-white post_status">@if($cuecard->status) Unpublish @else Publish @endif</span></td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

        @if(Session::has('alert-success')) 
            <style>
                .cuecards-table tbody tr:first-child *{
                  color: #000000 !important
                }
            </style>

        @endif




@stop