@extends('layouts/default')
@section('content')
<main>
    <div class="inner-wrapper">
        <img src="images/main-image_test.jpg">
        <div class="container">
            <div class="row">
        <div class="do-you-know">
            <h2>
                Do you know?
            </h2>
            <h4>
            @foreach($hows as $how)
                {{$how->doyouknow}}
            @endforeach
            </h4>
        </div>
            </div>
    </div>
    </div>
</main>
<section class="d-none" id="service-section" style="padding-bottom: 45px; background-color: #f0f8ff">
    <div class="inner-wrapper">
        <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-12  service">
                <a href="/ielts-reading">
                    <img src="images/reading.png" class="img-fluid">
                </a>
                <a href="/ielts-reading">
                    <h4 class="text-center py-3">
                        Reading
                    </h4>
                </a>
                <p class="text-center">
                Reading is the second part of the IELTS test, and takes 60 minutes. It consists of three...
                </p>
            </div>
            <div class="col-md-3 col-sm-12 service">
                <a href="/ielts-speaking">
                    <img src="images/speaking.png" class="img-fluid">
                </a>
                <a href="/ielts-speaking">
                    <h4 class="text-center py-3">
                        Speaking
                    </h4>
                </a>
                <p class="text-center">
                The IELTS Speaking test is designed to assess a wide range of skills. The examine...
                </p>
            </div>
            <div class="col-md-3 col-sm-12 service">
                <a href="/ielts-listening">
                    <img src="images/listening.png" class="img-fluid">
                </a>
                <a href="/ielts-listening">
                    <h4 class="text-center py-3">
                        Listening
                    </h4>
                </a>
                <p class="text-center">
                The IELTS Listening test will take about 30 minutes, and you will have an extra 10 min...
                </p>
            </div>
            <div class="col-md-3 col-sm-12 service">
                <a href="/ielts-writing">
                    <img src="images/writing.png" class="img-fluid">
                </a>
                <a href="/ielts-writing">
                    <h4 class="text-center py-3">
                        Writing
                    </h4>
                </a>
                <p class="text-center">
                The Academic Writing test is 60 minutes long. Candidates are required to write at least 150...
                </p>
            </div>
        </div>
    </div>
</div>
</section>

<section id="welcome" class="d-none">
    <div class="inner-wrapper">
        <div class="row">
            <h1>Welcome to <span>know your <span>ielts</span></span></h1>
            <p>
                It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
            </p>
        </div>
    </div>
</section>
<section class="d-none" id="how-to">
    <div class="inner-wrapper">
        <div class="row">
            <div class="how-box">
                How Box
            </div>
        </div>
    </div>
</section>
<section id="updates">
    <div class="inner-wrapper">
    <div class="container">
        <div class="row row-eq-height">
            @foreach($blogs as $blog)
            <div class="col-md-4 col-sm-12">
                <div class="blog-box h-100">
                    <div class="blog-image">
                        <img src="images/2-360x250.jpg">
                    </div>
                    <div class="blog-title">
                        <a href="/blog/{{$blog->slug}}">
                            <h4>
                                {{$blog->title}}
                            </h4>
                        </a>
                    </div>
                    <div class="blog-detail">
                        <p>
                        {{str_limit($blog->description, $limit = 150, $end = '...')}}
                        </p>
                    </div>
                    <div class="blog-action">
                        <a href="/blog/{{$blog->slug}}">Continue Blog Post</a>
                    </div>
                </div>
            </div>
           @endforeach
            <div class="col-md-4 col-sm-12">
                <div class="news-box h-100">
                    <div class="news-box-title">
                        <h6>Latest News</h6>
                    </div>
                    <div class="news-box-updates custom-scrollbar">
                        <ul>
                        @foreach($events as $event)                        
                            <li>
                                <a href="news-events/{{$event->slug}}">
                                    <p>
                                    {{$event->title}}
</p>
                                    <span>{{$event->created_at->diffForHumans()}}</span>
                                </a>
                            </li>
                        @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
@stop