@extends('layouts/default')
@section('content')
<nav aria-label="breadcrumb" class=" breadcrumb-bg">
    <div class="container">
        <ol class="breadcrumb rounded-0 m-0">
            <li class="breadcrumb-item "><a href="#" class="">Home</a></li>
            <li class="breadcrumb-item "><a href="#" class="">Useful Links</a></li>
        </ol>
    </div>
</nav>
<!-- <div class="page-title-bar">

</div> -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="">
                    <h5 class="mb-3">Useful Links</h5>
                    <table class="table table-striped">
                                <tbody>
                                    <tr class="text-left">
                                        <td class="w-100"><a href="">Link</a></td>
                                    </tr>
                                    <tr class="text-left">
                                        <td class="w-100"><a href="">Link</a></td>
                                    </tr>
                                    <tr class="text-left">
                                        <td class="w-100"><a href="">Link</a></td>
                                    </tr>
                                </tbody>
                            </table>
                </div>
                
            </div>
            <div class="col-md-4">
                <div class="card my-4">
                    <div class="card-body">
                        <img src="/images/300x600_ad.jpg" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
@stop