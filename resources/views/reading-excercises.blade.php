@extends('layouts/default')
@section('content')
<nav aria-label="breadcrumb" class="breadcrumb-bg">
    <div class="container">
    <ol class="breadcrumb rounded-0 m-0">
        <li class="breadcrumb-item "><a href="/">Home</a></li>
        <li class="breadcrumb-item "><a href="{{route('ieltsReading')}}">Reading</a></li>
        <li class="breadcrumb-item "><a href="{{route('readingExcercises')}}">Excercises</a></li>
    </ol>
    </div>
</nav>


@include('partials/interactive-elements/reading-interactive-elements-nav')

<section style="margin-top: -90px">
<div class="container">
    <div class="row">
        <div class="col-12">
    <h3 class="mb-3 w-100">IELTS Reading Excercises</h3>
    <table class="table">
  <!-- <thead>
    <tr>
      <th scope="col">Hits</th>
      <th scope="col">Title</th>
    </tr>
  </thead> -->
  <tbody>
      
    <tr>
        <td scope="row"><a href="/ielts-reading/reading-excercises/1">#1 Test</a></td>
      <td scope="row"><a href="/ielts-reading/reading-excercises/1"></a></td>
    </tr>
  
  </tbody>
</table>
        </div>
    </div>
</div>
</section>




@stop