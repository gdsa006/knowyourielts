@extends('layouts/default')
@section('content')
<nav aria-label="breadcrumb" class="breadcrumb-bg">
    <div class="container">
    <ol class="breadcrumb rounded-0 m-0">
        <li class="breadcrumb-item "><a href="/">Home</a></li>
        <li class="breadcrumb-item "><a href="{{route('ieltsSpeaking')}}">Speaking</a></li>
        <li class="breadcrumb-item "><a href="{{route('speakingCueCards')}}">Cue Cards</a></li>
    </ol>
    </div>
</nav>

<section id="main-course-section" style="padding-bottom: 45px; background-color: #f0f8ff">
    <div class="inner-wrapper">
        <div class="container">
        <div class="main-course-info">
            <h5 class="mb-3 text-center">Speaking Content</h5>
            <p class="text-center">The passage experienced a surge in popularity during the 1960s when Letraset used it on their dry-transfer sheets, and again during the 90s as desktop publishers bundled the text with their software. Today it's seen all around the web; on templates, websites, and stock designs.</p>
        </div>
            <div class="main-course-wrapper">
            <div class="row">
                <div class="col-md-4 col-sm-12 main-course text-center {{ (Request::is('*ielts-speaking/cue-cards') ? 'active' : '') }}">
                <a href="{{route('speakingCueCards')}}">
                    <div class="wrapper">
                            <i class="fa fa-headphones fa-4x" aria-hidden="true"></i>
                            <p class="text-center pt-3">
                                Cue Cards
                                </p>
                    </div>
                </a>
                </div>
                <div class="col-md-4 col-sm-12 main-course text-center">
                <a href="#">
                    <div class="wrapper">
                            <i class="fa fa-pencil-square fa-4x" aria-hidden="true"></i>
                            <p class="text-center pt-3">
                            Native Speaker
                            </p>
                    </div>
                </a>
                </div>
                <div class="col-md-4 col-sm-12 main-course text-center">
                    <a href="{{route('speakingTips')}}">
                    <div class="wrapper">
                            <i class="fa fa-lightbulb-o fa-4x" aria-hidden="true"></i>
                            <p class="text-center pt-3">
                                Speaking Tips
                                </p>
                    </div>
                    </a>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>


<section style="margin-top: -90px">
<div class="container">
    <div class="row">
        <div class="col-12">
    <h3 class="mb-3 w-100">IELTS Cue Card Topics</h3>
    <table class="table">
  <thead>
    <tr>
      <th scope="col">Topics</th>
      <th scope="col">Hits</th>
    </tr>
  </thead>
  <tbody>
      @foreach($cuecards as $cuecard)
    <tr>
        <td scope="row"><a href="/ielts-speaking/cue-cards/{{$cuecard->id}}">#{{$cuecard->id}} {{$cuecard->title}}</a></td>
      <td scope="row"><a href="/ielts-speaking/cue-cards/{{$cuecard->id}}">1005</a></td>
    </tr>
    @endforeach
  </tbody>

  <tfoot>
    <tr>
    <td  class="py-4"    colspan="">{{$cuecards->render()}}</td>

    </tr>
  </tfoot>
</table>
        </div>
    </div>
</div>
</section>




@stop