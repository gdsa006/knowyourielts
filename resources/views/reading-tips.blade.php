@extends('layouts/default')
@section('content')

<nav aria-label="breadcrumb" class="breadcrumb-bg">
    <div class="container">
        <ol class="breadcrumb rounded-0 m-0">
            <li class="breadcrumb-item "><a href="/">Home</a></li>
            <li class="breadcrumb-item "><a href="/ielts-reading/">Reading</a></li>
            <li class="breadcrumb-item "><a href="/ielts-reading/tips/">Tips</a></li>
        </ol>
    </div>
</nav>

@include('partials/interactive-elements/reading-interactive-elements-nav')


<section class="p-4 pt-0" style="margin-top: -60px; ">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h5 class="mb-3 text-center">Speaking Tips</h5>
                <p>Total Time is 60 min to solve 40 questions.</p>
                <p>We have 1 min 30 sec per Question. Time Management is important. Write answers directly to Answer sheets. Write answers in capital letters.</p>
                <p>Try not to attain knowledge from Reading article but keep things in check what is being informed in Comprehension.</p>
                <p>Every answer is in a sentence so, 40 answers are always in 40 sentences.</p>
                <p>Method to get answer in Reading :</p>
                <h6>Steps:</h6>
                <ol class="">
                    <li>Read the Question</li>
                    <li>Identify Keywords to the answer considering Noun, verb, adjective.</li>
                    <li>Locate Keyword (Scan with Pencil)</li>
                    <li>Translate the sentence to appropriate answer.</li>
                </ol>
                <p>Category of Questions: True/False/ Not Given</p>
                </div>
            
        </div>
    </div>
</section>
@stop