@extends('layouts/default')
@section('content')
<nav aria-label="breadcrumb" class="breadcrumb-bg">
    <div class="container">
    <ol class="breadcrumb rounded-0 m-0">
        <li class="breadcrumb-item "><a href="/">Home</a></li>
        <li class="breadcrumb-item "><a href="/ielts-reading/">Reading</a></li>
    </ol>
    </div>
</nav>
<section style="background: url(/images/reading-page-bg.jpg) #f5f5f5; background-position: right; background-repeat: no-repeat;">
<div class="container">
    <div class="row">
        <div class="col-7">
    <h3 class="mb-3 w-100">Reading Skills</h3>
    <p>The IELTS Listening test will take about 30 minutes, and you will have an extra 10 minutes to transfer your answers to the answer sheet.</p>
<p>The four parts of this practice Listening test are presented over four separate web pages. Make sure you move swiftly from one page to the next so that your practice is as realistic as possible.</p>
<p>For each part of the test, there will be time for you to look through the questions and time for you to check your answers.When you have completed all four parts of the Listening test you will have ten minutes to copy your answers on to a separate answer sheet.</p>
<a href="#" class="btn btn-primary rounded-0">Know More</a>
        </div>
    </div>
</div>
</section>

@include('partials/interactive-elements/reading-interactive-elements-nav')







@stop