<style>
    body{
        margin: 0;
        background: #87cefa;
    }
.container {
  height: 100vh;
  position: relative;
  padding: 0;
  margin: 0;
}

.vertical-center {
  margin: 0;
  position: absolute;
  top: 50%;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
  text-align: center;
  width: 100%;
}
</style>

<div class="container">
  <div class="vertical-center">
      <h1>🚧</h1>
    <h4>knowyourielts is under development. Please check back soon. Thank you!</h4>
  </div>
</div>  