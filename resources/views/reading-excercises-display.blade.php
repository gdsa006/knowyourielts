@extends('layouts/default')
@section('content')
<nav aria-label="breadcrumb" class="breadcrumb-bg">
  <div class="container">
    <ol class="breadcrumb rounded-0 m-0">
      <li class="breadcrumb-item "><a href="/">Home</a></li>
      <li class="breadcrumb-item "><a href="{{route('ieltsReading')}}">Reading</a></li>
      <li class="breadcrumb-item "><a href="{{route('readingExcercises')}}">Excercises</a></li>
    </ol>
  </div>
</nav>

<section class="py-0" id="reading-excercises-screens" style="position: fixed">
  <div class="container-fluid">
    <div class="row" id="results">
      @include('partials/speaking-model/details')
    </div>
  </div>
  </div>
</section>






<style>
  body {
    overflow: hidden
  }

  footer {
    display: none;
  }
</style>


@stop