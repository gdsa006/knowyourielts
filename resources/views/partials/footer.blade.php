<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-6 left">Copyright &copy; 2020 | Developed by GGWeb Solutions</div>
            <div class="col-md-6 right"><a href="/">Home</a><a href="" class="d-none">Terms of Use</a>&nbsp;|&nbsp;<a href="" class="d-none">Privacy</a></div>
        </div>
    </div>
</footer>