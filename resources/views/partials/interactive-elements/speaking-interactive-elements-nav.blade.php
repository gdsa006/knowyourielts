<section id="main-course-section" style="padding-bottom: 45px; background-color: #f0f8ff">
    <div class="inner-wrapper">
        <div class="container">
        <div class="main-course-info">
            <h5 class="mb-3 text-center">Speaking Content</h5>
            <p class="text-center">The passage experienced a surge in popularity during the 1960s when Letraset used it on their dry-transfer sheets, and again during the 90s as desktop publishers bundled the text with their software. Today it's seen all around the web; on templates, websites, and stock designs.</p>
        </div>
            <div class="main-course-wrapper">
            <div class="row">
                <div class="col-md-4 col-sm-12 main-course text-center {{ (Request::is('*ielts-speaking/cue-cards*') ? 'active' : '') }}">
                <a href="{{route('speakingCueCards')}}">
                    <div class="wrapper">
                            <i class="fa fa-headphones fa-4x" aria-hidden="true"></i>
                            <p class="text-center pt-3">
                                Cue Cards
                                </p>
                    </div>
                </a>
                </div>
                <div class="col-md-4 col-sm-12 main-course text-center {{ (Request::is('*ielts-speaking/cue-cards*') ? 'active' : '') }}">
                <a href="#">
                    <div class="wrapper">
                            <i class="fa fa-pencil-square fa-4x" aria-hidden="true"></i>
                            <p class="text-center pt-3">
                            Native Speaker
                            </p>
                    </div>
                </a>
                </div>
                <div class="col-md-4 col-sm-12 main-course text-center {{ (Request::is('*ielts-speaking/tips*') ? 'active' : '') }}">
                    <a href="{{route('speakingTips')}}">
                    <div class="wrapper">
                            <i class="fa fa-lightbulb-o fa-4x" aria-hidden="true"></i>
                            <p class="text-center pt-3">
                                Speaking Tips
                                </p>
                    </div>
                    </a>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>