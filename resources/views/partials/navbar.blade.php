<div class="alert alert-info text-center" id="success-alert">
  <button type="button" class="close" data-dismiss="alert">x</button>
  We'll be adding more content very soon to help students with IELTS content. <strong>Check back soon!</strong>
</div>
<header>
        <div class="main-header">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 align-items-center">
                    <a href="/">
                        <h2 class="font-weight-bold align-items-center h-100">know your <span>ielts</span><span class="tag-line">your <span>kyi</span> to success...</span></h2>
                    </a>
                    <button style="position: absolute; top: 14px; right: 0" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon">   
    <i class="fas fa-bars" style="color:#007bff; font-size:28px;"></i>
</span>
						</button>

                </div>
                <div class="col-md-8 col-sm-12 d-none d-sm-block scroll">
                    <nav class="nav">
                        <a class="nav-link {{ (Request::is('*blog*') ? 'active' : '') }}" href="/blog">Blog</a>
                        <a class="nav-link {{ (Request::is('*news-events*') ? 'active' : '') }}" href="/news-events">News & Events</a>
                        <a class="nav-link {{ (Request::is('*contact-us') ? 'active' : '') }}" href="/contact-us">Contact Us</a>
                        <a class="d-none nav-link {{ (Request::is('*about-us') ? 'active' : '') }}" href="/about-us">About Us</a>
                        <a class="nav-link {{ (Request::is('*ielts-calculator') ? 'active' : '') }}" href="/ielts-calculator">IELTS Calculator</a>
                    </nav>
                </div>
                
            </div>
        </div>
        </div>

    <nav class="navbar navbar-expand-lg bg-white navbar-custom" data-toggle="sticky-onscroll">
        <div class="container">
        <a class="navbar-brand logo" href="" target="_blank">ky<span>i</span></a>	
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item {{ (Request::is('*/') ? 'active' : '') }}">
                        <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="d-none nav-item dropdown {{ (Request::is('*ielts-listening*') ? 'active' : '') }} {{ (Request::is('*ielts-writing*') ? 'active' : '') }} {{ (Request::is('*ielts-reading*') ? 'active' : '') }}{{ (Request::is('*ielts-speaking*') ? 'active' : '') }}">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Prepare & Practice
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item {{ (Request::is('*ielts-reading*') ? 'active' : '') }}" href="/ielts-reading/">Reading</a>
                            <a class="dropdown-item {{ (Request::is('*ielts-speaking*') ? 'active' : '') }}" href="/ielts-speaking/">Speaking</a>
                            <a class="dropdown-item {{ (Request::is('*ielts-listening*') ? 'active' : '') }}" href="/ielts-listening/">Listening</a>
                            <a class="dropdown-item {{ (Request::is('*ielts-writing*') ? 'active' : '') }}" href="/ielts-writing/">Writing</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown d-none">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        About IELTS
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">General Information</a>
                            
                        </div>
                    </li>
                    <li class="d-none nav-item dropdown {{ (Request::is('*resources*') ? 'active' : '') }}">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Resources
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item {{ (Request::is('*documents-downloads') ? 'active' : '') }}" href="{{route('documentsDownloads')}}">Document Download</a>
                            <a class="dropdown-item" href="#">Test Format</a>
                            <a class="dropdown-item" href="#">Tips</a>
                            <a class="dropdown-item" href="#">How To Prepare</a>
                            <a class="dropdown-item {{ (Request::is('*useful-links') ? 'active' : '') }}" href="{{route('answerKeys')}}">Answer Keys</a>
                            <a class="dropdown-item" href="#">Personal Experiences</a>
                            <a class="dropdown-item {{ (Request::is('*useful-links') ? 'active' : '') }}" href="{{route('usefulLinks')}}">Useful Links</a>
                            <a class="dropdown-item {{ (Request::is('*ielts-word-counter') ? 'active' : '') }}" href="{{route('wordCounter')}}">Word Counter</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" target="_blank" href="https://www.britishcouncil.in/exam/ielts/book-test">Book a Test</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" target="_blank" href="https://ielts.britishcouncil.org/CheckResults.aspx">Results</a>
                    </li>
                </ul>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item social-media">
                        <a href="https://www.facebook.com/knowyour.ielts"><i class="fab fa-facebook-f"></i></a>
                        <a href="https://www.twitter.com/knowyourielts"><i class="fab fa-twitter"></i></a>
                        <a href="https://www.youtube.com/channel/knowyourielts"><i class="fab fa-youtube"></i></a>
                    </li>
                </ul>
            </div>
    </nav>
    </div>
</header>
