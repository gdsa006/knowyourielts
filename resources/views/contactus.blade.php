@extends('layouts/default')
@section('content')
<nav aria-label="breadcrumb" class=" breadcrumb-bg">
    <div class="container">
        <ol class="breadcrumb rounded-0 m-0">
            <li class="breadcrumb-item "><a href="/" class="">Home</a></li>
            <li class="breadcrumb-item "><a href="/contact-us" class="">Contact Us</a></li>
        </ol>
    </div>
</nav>


<section id="contactus">
    <div class="container">
        <div class="row row-eq-height">
            <div class="col-md-8">
                <h5 class="mb-3">Contact Us</h5>
                <p>We are here to answer any questions you may have about our website experience. Reach out to us we'll respond as soon as we can.</p>
                <p>Even if there is something you have always wanted to experience and can't find it on knowyourielts, let us know and we promise we'll do our best.</p>
                {{ Form::open(array('url' => '/contact/sendmail', 'class' => 'sendemail', 'name' => 'sendemail', 'files' => 'false', 'enctype' => 'multipart/form-data' )) }}
                <div class="row">
                    <div class="form-group col">
                        <label for="formGroupExampleInput">Name</label>
                        <input type="text" class="form-control cInput" id="formGroupExampleInput" placeholder="" name="name">
                        <span id="name-error" class="text-danger"></span>
                    </div>
                    <div class="form-group col">
                        <label for="formGroupExampleInput">Email</label>
                        <input type="text" class="form-control cInput" id="formGroupExampleInput" placeholder="" name="email">
                        <span id="email-error" class="text-danger"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col">
                        <label for="formGroupExampleInput">Message</label>
                        <textarea class="form-control cInput" id="formGroupExampleInput" placeholder="" rows="10"  name="message"></textarea>
                        <span id="message-error" class="text-danger"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col mb-0">
                    <input type="submit" class="btn btn-primary float-right" value="Send" id="send-contact-email">
                    <span class="text-success confirmMail float-right mr-3 mt-2">Message Sent!</span>
                    <input type="submit" class="btn btn-primary float-right" disabled value="Sending..." id="sending-contact-email">
                    </div>
                </div>
                {{ Form::close() }}
            </div>
            <div class="col-md-4 contact-sidebar d-sm-none d-md-block">
                <div class="inner-wrapper">
                    <div class="blk">
                        <h6>Email</h6>
                        <p>gdsa006@gmail.com</p>
                    </div>
                    <div class="blk">
                        <h6>Facebook</h6>
                        <p class="social-media"><a href="#">facebook.com/knowyour.ielts</a></p>
                    </div>
                    <div class="blk">
                        <h6>Twitter</h6>
                        <p class="social-media"><a href="#">twitter.com/knowyourielts</a></p>
                    </div>
                    <div class="blk">
                        <h6>Youtube</h6>
                        <p class="social-media"><a href="#">youtube.com/channel/knowyourielts</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop