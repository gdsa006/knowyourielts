@extends('layouts/default')
@section('content')
<nav aria-label="breadcrumb" class=" breadcrumb-bg">
    <div class="container">
        <ol class="breadcrumb rounded-0 m-0">
            <li class="breadcrumb-item "><a href="/" class="">Home</a></li>
            <li class="breadcrumb-item "><a href="/about-us" class="">About Us</a></li>
        </ol>
    </div>
</nav>
<section>
    <div class="container">
    <div class="row">
        <h1 class="w-100 text-center">Our purpose is to help you getting higher bands in IELTS!</h1>
        <div class="row row-eq-height pt-5">
            <div class="col-6">
                <img src="images/close-up-photography-of-yellow-green-red-and-brown-plastic-163064.jpg" class="img-fluid">
            </div>
            <div class="col-6">
                <h5>What We Do</h5>
            Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type specimen book. It usually begins with:

“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.”
The purpose of lorem ipsum is to create a natural looking block of text (sentence, paragraph, page, etc.) that doesn't distract from the layout. A practice not without controversy, laying out pages with meaningless filler text can be very useful when the focus is meant to be on design, not content.

The passage experienced a surge in popularity during the 1960s when Letraset used it on their dry-transfer sheets, and again during the 90s as desktop publishers bundled the text with their software. Today it's seen all around the web; on templates, websites, and stock designs. Use our generator to get your own, or read on for the authoritative history of lorem ipsum.
            </div>
        </div>
        <div class="row pt-5 text-center">
            <h4 class="text-center">Is there is something you have always wanted to experience and can't find it on knowyourielts, let us know and we promise we'll do our best.</h3>
            <a href="/contact-us/" class="btn btn-primary rounded-0 d-block mx-auto">Contact Us Now</a>
        </div>
    </div>
    </div>
</section>

@stop