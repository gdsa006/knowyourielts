@extends('layouts/default')
@section('content')
<nav aria-label="breadcrumb" class="breadcrumb-bg">
    <div class="container">
    <ol class="breadcrumb rounded-0 m-0">
        <li class="breadcrumb-item "><a href="/">Home</a></li>
        <li class="breadcrumb-item "><a href="/ielts-listening/">Listening</a></li>
    </ol>
    </div>
</nav>
<section style="background: url(/images/listening-page-bg.jpg) #f5f5f5; background-position: right; background-repeat: no-repeat;">
<div class="container">
    <div class="row">
        <div class="col-7">
    <h3 class="mb-3 w-100">Listening Skills</h3>
    <p>The IELTS Listening test will take about 30 minutes, and you will have an extra 10 minutes to transfer your answers to the answer sheet.</p>
<p>The four parts of this practice Listening test are presented over four separate web pages. Make sure you move swiftly from one page to the next so that your practice is as realistic as possible.</p>
<p>For each part of the test, there will be time for you to look through the questions and time for you to check your answers.When you have completed all four parts of the Listening test you will have ten minutes to copy your answers on to a separate answer sheet.</p>
<a href="#" class="btn btn-primary rounded-0">Know More</a>
        </div>
    </div>
</div>
</section>

<section id="main-course-section" style="padding-bottom: 45px; background-color: #f0f8ff">
    <div class="inner-wrapper">
        <div class="container">
        <div class="main-course-info">
            <h5 class="mb-3 text-center">Listening Content</h5>
            <p class="text-center">The passage experienced a surge in popularity during the 1960s when Letraset used it on their dry-transfer sheets, and again during the 90s as desktop publishers bundled the text with their software. Today it's seen all around the web; on templates, websites, and stock designs.</p>
        </div>
            <div class="main-course-wrapper">
            <div class="row">
                <div class="col-md-4 col-sm-12 main-course text-center">
                <a href="listening-samples">
                    <div class="wrapper">
                            <i class="fa fa-headphones fa-4x" aria-hidden="true"></i>
                            <p class="text-center pt-3">
                                Listening Samples
                                </p>
                    </div>
                </a>
                </div>
                <div class="col-md-4 col-sm-12 main-course text-center">
                <a href="#">
                    <div class="wrapper">
                            <i class="fa fa-pencil-square fa-4x" aria-hidden="true"></i>
                            <p class="text-center pt-3">
                                Listening Excercises
                            </p>
                    </div>
                </a>
                </div>
                <div class="col-md-4 col-sm-12 main-course text-center">
                    <a href="{{route('listeningTips')}}">
                    <div class="wrapper">
                            <i class="fa fa-lightbulb-o fa-4x" aria-hidden="true"></i>
                            <p class="text-center pt-3">
                                Tips
                                </p>
                    </div>
                    </a>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>






@stop