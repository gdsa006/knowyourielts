@extends('layouts/default')
@section('content')
<nav aria-label="breadcrumb" class=" breadcrumb-bg">
    <div class="container">
  <ol class="breadcrumb rounded-0 m-0">
    <li class="breadcrumb-item "><a href="#" class="">Home</a></li>
    <li class="breadcrumb-item "><a href="#" class="">IELTS Calculator</a></li>
  </ol>
    </div>
</nav>
<!-- <div class="page-title-bar">

</div> -->


<section>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="">
                    <h5 class="mb-3">Calculate your General IELTS Score</h5>
                    <p>
                    You will be given a score from 1 to 9 for each part of the test (Listening, Reading, Writing and Speaking). The average produces your overall band score. You can score whole (like 5.0, 6.0) or half (like 6.5, 7.5) bands in each part of the test.
                    </p>
                    <p>
                    Input your possible scores for each part of the exam to get the estimate overall score.
                    </p>
                </div>
                <div class="form-group">
                    <label for="sel1">Listening</label>
                    <input type="range" class="form-control-range range-slider__range listeningVal" id="formControlRange" min='0' max='9' step='.25'>
                    <span class="range-slider__value">4.5</span>
                </div>
                <div class="form-group">
                    <label for="sel1">Reading</label>
                    <input type="range" class="form-control-range range-slider__range readingVal" id="formControlRange" min='0' max='9' step='.25'>
                    <span class="range-slider__value">4.5</span>
                </div>
                <div class="form-group">
                    <label for="sel1">Writing</label>
                    <input type="range" class="form-control-range range-slider__range writingVal" id="formControlRange" min='0' max='9' step='.25'>
                    <span class="range-slider__value">4.5</span>
                </div>
                <div class="form-group range-slider">
                    <label for="sel1">Speaking</label>
                    <input type="range" class="form-control-range range-slider__range speakingVal" id="formControlRange" min='0' max='9' step='.25'>
                    <span class="range-slider__value">4.5</span>
                </div>
                <div class="form-group">
                    <button type="button" class="btn btn-success" id="calculate">Calculate<img src="images/loading.gif" width="25" id="result_loading_gif"></button><p id="final-result"  style='font-weight: bold; color: darkgreen; float: right'></p>
                </div>
                </div>
            
            <div class="col-md-4">
            <div class="card my-4">
          <div class="card-body">
          <img src="/images/300x600_ad.jpg" class="img-fluid">
          </div>
        </div>
</div>
            </div>
        </div>
    </div>
</section>








@stop