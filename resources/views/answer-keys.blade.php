@extends('layouts/default')
@section('content')
<nav aria-label="breadcrumb" class=" breadcrumb-bg">
    <div class="container">
        <ol class="breadcrumb rounded-0 m-0">
            <li class="breadcrumb-item "><a href="#" class="">Home</a></li>
            <li class="breadcrumb-item "><a href="#" class="">Document Downloads</a></li>
        </ol>
    </div>
</nav>
<!-- <div class="page-title-bar">

</div> -->


<section>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active rounded-0" id="documents-tab" data-toggle="tab" href="#documents" role="tab" aria-controls="home" aria-selected="true">Answer Keys</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="documents" role="tabpanel" aria-labelledby="home-tab">
                        <div class="">
                            <table class="table table-striped">
                                <tbody>
                                    @foreach($answerkeys as $answerkey)
                                    <tr class="text-center">
                                        <td class="w-50">#{{$answerkey->id}} Answer Keys</td>
                                        <td class="w-50"><a href="">{{$answerkey->exam}}</a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <img src="/images/300x600_ad.jpg" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>








@stop