<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswerkeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answerkeys', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('answerkeys', 2000);
            $table->string('category', 100);
            $table->string('test', 100);
            $table->string('exam', 100);
            $table->string('test_date', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answerkeys');
    }
}
