<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'maintainance');


// Route::get('/kyi', array('uses' => 'HomeController@index'));
Route::get('/', array('uses' => 'HomeController@index'));


Route::get('/ielts-listening', array('uses' => 'CourseController@listening', 'as' => 'ieltsListening'));
Route::get('/ielts-listening/tips', array('uses' => 'CourseController@listeningTips', 'as' => 'listeningTips'));
Route::get('/ielts-listening/listening-samples', array('uses' => 'CourseController@listeningSamples'));

Route::get('/ielts-writing', array('uses' => 'CourseController@writing', 'as' => 'ieltsWriting'));
Route::get('/ielts-writing/tips', array('uses' => 'CourseController@writingTips', 'as' => 'writingTips'));

Route::get('/ielts-speaking', array('uses' => 'CourseController@speaking', 'as' => 'ieltsSpeaking'));
Route::get('/ielts-speaking/tips', array('uses' => 'CourseController@speakingTips', 'as' => 'speakingTips'));
Route::get('/ielts-speaking/cue-cards', array('uses' => 'CourseController@speakingCueCards', 'as' => 'speakingCueCards'));
Route::get('/ielts-speaking/cue-cards/{id}', array('uses' => 'CourseController@speakingCueCardsDisplay'));

Route::get('ielts-reading', array('uses' => 'CourseController@reading', 'as' => 'ieltsReading'));
Route::get('ielts-reading/tips', array('uses' => 'CourseController@readingTips', 'as' => 'readingTips'));
Route::get('ielts-reading/reading-excercises', array('uses' => 'CourseController@readingExcercises', 'as' => 'readingExcercises'));
Route::get('ielts-reading/reading-excercises/{id}', array('uses' => 'CourseController@readingExcercisesDisplay', 'as' => 'readingExcercisesDisplay'));

Route::get('/ielts-calculator', array('uses' => 'CalculatorController@index'));
Route::get('resources/ielts-word-counter', array('uses' => 'WordcounterController@index', 'as' => 'wordCounter'));
Route::get('resources/documents-downloads', array('uses' => 'DocumentsdownloadsController@index', 'as' => 'documentsDownloads'));
Route::get('resources/useful-links', array('uses' => 'UsefullinksController@index', 'as' => 'usefulLinks'));

Route::get('/news-events', array('uses' => 'NewsEventsController@index'));
Route::get('/news-events/{slug}', array('uses' => 'NewsEventsController@details'));

Route::get('/contact-us', array('uses' => 'ContactController@index'));
Route::post('/sendmail', array('uses' => 'ContactController@sendMail', 'as' => 'sendmail'));
Route::get('/about-us', array('uses' => 'AboutController@index'));

Route::get('/blog', array('uses' => 'BlogController@index'));
Route::get('/blog/{slug}', array('uses' => 'BlogController@details'));

Route::get('/answer-keys', array('uses' => 'AnswerkeysController@index', 'as' => 'answerKeys'));

Route::post('/calcdata', array('uses' => 'CalculatorController@store', 'as' => 'calcdata'));

Route::post('login', array('uses' => 'UsersController@doLogin', 'as' => 'login'));
Route::get('logout', array('uses' => 'UsersController@doLogout', 'as' => 'logout'));

Route::get('admin', array('uses' => 'DashboardController@showLogin', 'as' => 'showLogin'));
Route::get('admin/dashboard', array('uses' => 'DashboardController@dashboard', 'as' => 'adminDashboard'))->middleware('auth');
Route::get('admin/speaking/cue-cards', array('uses' => 'DashboardController@adminSpeakingCueCards', 'as' => 'adminSpeakingCueCards'))->middleware('auth');
Route::get('admin/speaking/cue-cards/add', array('uses' => 'DashboardController@adminSpeakingCueCardsAdd', 'as' => 'adminSpeakingCueCardsAdd'))->middleware('auth');
Route::post('admin/speaking/cue-cards/store', array('uses' => 'DashboardController@adminSpeakingCueCardsStore', 'as' => 'adminSpeakingCueCardsStore'))->middleware('auth');
Route::get('admin/speaking/cue-cards/edit/{id}', array('uses' => 'DashboardController@adminSpeakingCueCardsEdit', 'as' => 'adminSpeakingCueCardsEdit'))->middleware('auth');
Route::post('admin/speaking/cue-cards/update/{id}', array('uses' => 'DashboardController@adminSpeakingCueCardsUpdate', 'as' => 'adminSpeakingCueCardsUpdate'))->middleware('auth');

Route::get('admin/blogs', array('uses' => 'DashboardController@adminBlogs', 'as' => 'adminBlogs'))->middleware('auth');
Route::get('admin/blogs/add', array('uses' => 'DashboardController@adminBlogsAdd', 'as' => 'adminBlogsAdd'))->middleware('auth');
Route::post('admin/blogs/store', array('uses' => 'DashboardController@adminBlogsStore', 'as' => 'adminBlogsStore'))->middleware('auth');
Route::get('admin/blogs/edit/{id}', array('uses' => 'DashboardController@adminBlogsEdit', 'as' => 'adminBlogsEdit'))->middleware('auth');
Route::post('admin/blogs/update/{id}', array('uses' => 'DashboardController@adminBlogsUpdate', 'as' => 'adminBlogsUpdate'))->middleware('auth');

Route::get('admin/news-events', array('uses' => 'DashboardController@adminNewsEvents', 'as' => 'adminNewsEvents'))->middleware('auth');
Route::get('admin/news-events/add', array('uses' => 'DashboardController@adminNewsEventsAdd', 'as' => 'adminNewsEventsAdd'))->middleware('auth');
Route::post('admin/news-events/store', array('uses' => 'DashboardController@adminNewsEventsStore', 'as' => 'adminNewsEventsStore'))->middleware('auth');
Route::get('admin/news-events/edit/{id}', array('uses' => 'DashboardController@adminNewsEventsEdit', 'as' => 'adminNewsEventsEdit'))->middleware('auth');
Route::post('admin/news-events/update/{id}', array('uses' => 'DashboardController@adminNewsEventsUpdate', 'as' => 'adminNewsEventsUpdate'))->middleware('auth');

Route::get('admin/answer-keys', array('uses' => 'DashboardController@adminAnswerKeys', 'as' => 'adminAnswerKeys'))->middleware('auth');
Route::get('admin/answer-keys/add', array('uses' => 'DashboardController@adminAnswerKeysAdd', 'as' => 'adminAnswerKeysAdd'))->middleware('auth');
Route::post('admin/answer-keys/store', array('uses' => 'DashboardController@adminAnswerKeysStore', 'as' => 'adminAnswerKeysStore'))->middleware('auth');
Route::get('admin/answer-keys/edit/{id}', array('uses' => 'DashboardController@adminAnswerKeysEdit', 'as' => 'adminAnswerKeysEdit'))->middleware('auth');
Route::post('admin/answer-keys/update/{id}', array('uses' => 'DashboardController@adminAnswerKeysUpdate', 'as' => 'adminAnswerKeysUpdate'))->middleware('auth');

Route::post('admin/uploadpic', array('uses' => 'DashboardController@adminUploadPic', 'as' => 'adminUploadPic'))->middleware('auth');
Route::post('admin/updatepoststatus', array('uses' => 'DashboardController@updatePostStatus', 'as' => 'updatePostStatus'))->middleware('auth');

