(function($) {
  "use strict"; // Start of use strict

  // Toggle the side navigation
  $("#sidebarToggle, #sidebarToggleTop").on('click', function(e) {
    $("body").toggleClass("sidebar-toggled");
    $(".sidebar").toggleClass("toggled");
    if ($(".sidebar").hasClass("toggled")) {
      $('.sidebar .collapse').collapse('hide');
    };
  });

  // Close any open menu accordions when window is resized below 768px
  $(window).resize(function() {
    if ($(window).width() < 768) {
      $('.sidebar .collapse').collapse('hide');
    };
  });

  // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
  $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function(e) {
    if ($(window).width() > 768) {
      var e0 = e.originalEvent,
        delta = e0.wheelDelta || -e0.detail;
      this.scrollTop += (delta < 0 ? 1 : -1) * 30;
      e.preventDefault();
    }
  });

  // Scroll to top button appear
  $(document).on('scroll', function() {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
      $('.scroll-to-top').fadeIn();
    } else {
      $('.scroll-to-top').fadeOut();
    }
  });

  // Smooth scrolling using jQuery easing
  $(document).on('click', 'a.scroll-to-top', function(e) {
    var $anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: ($($anchor.attr('href')).offset().top)
    }, 1000, 'easeInOutExpo');
    e.preventDefault();
  });

})(jQuery); // End of use strict


$(document).ready(function () {
  //@naresh action dynamic childs
  var next = 1;
  $("#add-more").click(function(e){
      e.preventDefault();
      var addto = "#model" + next;
      var addRemove = "#model" + (next);
      next = next + 1;
      var newIn = ' <div id="model'+ next +'" class="form-group"><label id="model-label'+ next +'">Model ' + next + '</label><textarea name="model[]" class="form-control summernote" rows="8"></textarea></div>';
   
      var newInput = $(newIn);
      var removeBtn = '<button id="remove' + (next - 1) + '" class="btn btn-danger float-right btn-circle btn-sm remove-me" ><i class="fas fa-trash"></i></button></div></div><div id="model">';
      var removeButton = $(removeBtn);
      $(addto).after(newInput);
      $('#model-label'+ (next - 1)).after(removeButton);
      $("#model" + next).attr('data-source',$(addto).attr('data-source'));
      $("#count").val(next);  
      $('.summernote').summernote({
        placeholder: '',
        tabsize: 1,
        height: 100,
        toolbar: [
          // [groupName, [list of button]]
          ['para', ['ul', 'ol', 'paragraph']],

        ]
      });
          $('.remove-me').click(function(e){
              e.preventDefault();
              var modelNum = this.id.charAt(this.id.length-1);
              var modelID = "#model" + modelNum;
              $(this).remove();
              $(modelID).remove();
          });
  });

});



$(".uploadPic").on('change', function(event) {
  event.preventDefault();
  $('.uploadPicForm').submit();
});

$('.uploadPicForm').on('submit', function(e){
  e.preventDefault();
  var form = document.forms.namedItem("uploadPicForm");
  $.ajax(
      {
          async: true,
          url: adminUploadPic,
          method: "POST",
          data: new FormData(form),
          contentType: false,
          cache: false,
          processData: false,
          success:function(data)
          {
              console.log(data);
              $('#message').css('display', 'block');
              $('#message').html(data.message);
              $('#message').addClass(data.class_name);
              $('.thumbnail').html(data.uploaded_image);
              $('.thumbnail').css('display', 'block');
              $('#file_name').val(data.file_name);
              $('#file').html(data.message);
          }
      });
});

