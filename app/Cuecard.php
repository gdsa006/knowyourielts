<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Cuecard extends Authenticatable
{
    
	protected $table = 'cuecards';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function cuecardmodels()
	{
		return $this->hasMany(Cuecardmodel::class, 'cuecards_id');
	}
}
