<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;

class WordcounterController extends Controller
{
    public function index(){
        return View::make('word-counter');
    }
}
