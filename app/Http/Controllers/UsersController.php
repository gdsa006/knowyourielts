<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use App\User;
use Illuminate\Support\Facades\Validator;   
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Session;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Response;

class UsersController extends Controller
{
    public function doLogin(Request $request)
    {
        $remember = (Input::has('remember')) ? true : false;
        
		$validator = Validator::make($request->all(), [
            'email'    => 'required|email', // make sure the email is an actual email
            'password' => 'required' // password can only be alphanumeric and has to be greater than 3 characters
        ]);
		if ($validator->fails()) {
            return Redirect::to('admin')
                ->withErrors($validator) // send back all errors to the login form
                ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
        }
        else {
             // create our user data for the authentication
            $userdata = array(
                'email'     => request()->input('email'),
                'password'  => request()->input('password')
            );
            // attempt to do the login
            if (Auth::attempt($userdata)) {
                // validation successful!
                // redirect them to the secure section or whatever
                // return Redirect::to('secure');
                // for now we'll just echo success (even though echoing in a controller is bad)
                echo 'LOGIN SUCCESS';
                return Redirect::to('/admin/dashboard');
            } 
            else 
            {        
                // validation not successful, send back to form 
				return Redirect::to('admin');
            }
        }
	}
	
	public function doLogout()
    {
        //Auth::logout(); // log the user out of our application // not working properly
        Session::flush();
        return Redirect::to('admin'); // redirect the user to the login screen
    }
}
