<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use View;
use App\Event;

class NewsEventsController extends Controller
{
    public function index(){
        $event = new Event();
        $events = $event->where('status', 1)->orderBy('created_at', 'ASC')->get();

        return View::make('news-events', compact('events'));
    }

    public function details($slug){
        $eventdetail = Event::where('slug', $slug)->first();
        return View::make('news-events-view', compact('eventdetail'));
    }
}
