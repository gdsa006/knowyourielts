<?php

namespace App\Http\Controllers;
use View;
use App\Cuecard;
use App\Cuecardmodel;
use App\Blog;
use App\Event;
use App\Hit;
use App\Answerkey;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;   
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class DashboardController extends Controller
{

    public function showLogin(){
        return View::make('admin/users/login', compact('')); 
    }

    public function dashboard(){
        $cuecard = new Cuecard();
        

        // SPEAKING
        $total_cuecard = $cuecard->whereMonth('created_at', Carbon::now()->format('m'))->count();
        $total_nativespeaker = 5;
        $speaking_total = $total_cuecard + $total_nativespeaker; //SPEAKING

        // READING
        $total_1 = 4;
        $total_2 = 7;
        $reading_total = $total_1 + $total_1; //READING

        // LISTENING
        $total_1 = 3;
        $total_2 = 6;
        $listening_total = $total_1 + $total_1; //LISTENING

        // WRITING
        $total_1 = 4;
        $total_2 = 5;
        $writing_total = $total_1 + $total_1; //WRITING

        $course_content = $speaking_total + $reading_total + $listening_total + $writing_total;

        $hit = new Hit();

        $hits = Hit::select('id', 'created_at')->whereYear('created_at', Carbon::now()->format('Y'))->get()->groupBy(function($date) {
            return Carbon::parse($date->created_at)->format('m');
        });

        $hitcount = [];
        $hitArr = [];
        
        foreach ($hits as $key => $value) {
            $hitcount[(int)$key] = count($value);
        }

        for($i = 1; $i <= 12; $i++){
            if(!empty($hitcount[$i])){
                $hitArr[$i] = $hitcount[$i];    
            }
            else{
                $hitArr[$i] = 0; 
            }
        }

        $hits = $hitArr;
        return View::make('admin/dashboard', compact('course_content', 'speaking_total', 'listening_total', 'writing_total', 'reading_total', 'listening_total', 'writing_total', 'hits')); 
    }

    public function adminSpeakingCueCards(){
        $cuecard = new Cuecard();
        $cuecards = $cuecard->orderBy('created_at', 'DSC')->get();
        return View::make('admin/cue-cards', compact('cuecards')); 
    }

    public function adminSpeakingCueCardsAdd(){
        return View::make('admin/add-cue-card', compact('')); 
    }

    public function adminSpeakingCueCardsStore(Request $request){
        $title = request()->input('title');
		$what_to_say = request()->input('what_to_say');
        $explain_what = request()->input('explain_what');
        $model   = request()->input('model', []);
		$rules = ([
			'title'    => 'required',
			'what_to_say'    => 'required',
			'explain_what' => 'required'
        ]);

        $validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
			return Redirect::back()->withInput()->with('alert-danger', 'The data wasn not saved successfully')->withErrors($validator);
		}
		else{
                $cuecard = new Cuecard();
				$cuecard->title = $title;
				$cuecard->say = $what_to_say;
                $cuecard->explain = $explain_what;
                $cuecard->save();
                foreach($model as $m) { 
                    $mod = new Cuecardmodel(['model' => $m]);
                    echo $mod;
                    $cuecard_last = Cuecard::where('id', $cuecard->id)->first(); 
                    $cuecard_last->cuecardmodels()->save($mod); 
                }
                return Redirect::to('admin/speaking/cue-cards/')->withInput()->with('alert-success', 'The data saved successfully');
		}
    }

    public function adminSpeakingCueCardsEdit($id){
        $cuecard = new Cuecard();
        $cuecard= $cuecard->with([
            'cuecardmodels' => function ($query) use ($id) {
                $query->where('cuecards_id', $id);
            },
        ])->where('id', $id)->first();
        return View::make('admin/edit-cue-card', compact('cuecard')); 
    }

    public function adminSpeakingCueCardsUpdate($id, Request $request){
        $title = request()->input('title');
		$what_to_say = request()->input('what_to_say');
        $explain_what = request()->input('explain_what');
        $model   = request()->input('model', []);
		$rules = ([
			'title'    => 'required',
			'what_to_say'    => 'required',
			'explain_what' => 'required'
        ]);

        $validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
			return Redirect::back()->withInput()->with('alert-danger', 'The data wasn not saved successfully')->withErrors($validator);
		}
		else{
                $cuecard = new Cuecard();
                $cuecards = $cuecard->find($id);
				$cuecards->title = $title;
				$cuecards->say = $what_to_say;
                $cuecards->explain = $explain_what;
                $cuecards->update();
                $cuecard_last = $cuecards->cuecardmodels()->get(); 
                //echo '->'.$cuecard_last[0] . '<br>';
                    foreach($model as $i=>$m) { 
                        //$mod = new Cuecardmodel(['model' => $m]);
                        $cuecard_last[$i]->update(['model' => $m]);
                    } 
                return Redirect::to('admin/speaking/cue-cards/')->withInput()->with('alert-success', 'The data saved successfully');
               
            }
        }

        public function adminBlogs(){
            $blog = new Blog();
            $blogs = $blog->orderBy('created_at', 'DSC')->get();
            return View::make('admin/blogs', compact('blogs')); 
        }

        public function adminBlogsAdd(){
            return View::make('admin/add-blog', compact('')); 
        }

        public function adminBlogsStore(Request $request){
            $image = request()->input('image');
            $title = request()->input('title');
            $slug = request()->input('slug');
            $description = request()->input('description');
            $author = request()->input('author');
            $tags = request()->input('tags');
            $keywords = request()->input('keywords');
            $rules = ([
                'title'    => 'required',
                'slug'    => 'required',
                'description' => 'required',
                'author' => 'required',
                'tags' => 'required',
                'keywords' => 'required',
            ]);
    
            $validator = Validator::make($request->all(), $rules);
    
            if ($validator->fails()) {
                return Redirect::back()->withInput()->with('alert-danger', 'The data wasn not saved successfully')->withErrors($validator);
            }
            else{
                    $cuecard = new Blog();
                    $cuecard->image = $image;
                    $cuecard->title = $title;
                    $cuecard->slug = $slug;
                    $cuecard->description = $description;
                    $cuecard->author = $author;
                    $cuecard->tags = $tags;
                    $cuecard->keywords = $keywords;
                    $cuecard->save();
                    return Redirect::to('admin/blogs')->withInput()->with('alert-success', 'The data saved successfully');
            }
        }

        public function adminBlogsEdit($id){
            $blog = new Blog();
            $blog = $blog->find($id);
            return View::make('admin/edit-blog', compact('blog')); 
        }

        public function adminBlogsUpdate($id, Request $request){
            $image = request()->input('image');
            $title = request()->input('title');
            $slug = request()->input('slug');
            $description = request()->input('description');
            $author = request()->input('author');
            $tags = request()->input('tags');
            $keywords = request()->input('keywords');
            $blog = new Blog();
            $blogs = $blog->find($id);
            $rules = ([
                'title'    => 'required',
                'slug'    => 'required',
                'description' => 'required',
                'author' => 'required',
                'tags' => 'required',
                'keywords' => 'required',
            ]);
    
            $validator = Validator::make($request->all(), $rules);
    
            if ($validator->fails()) {
                return Redirect::back()->withInput()->with('alert-danger', 'The data wasn not saved successfully')->withErrors($validator);
            }
            else{
                    $blogs->image = $image;
                    $blogs->title = $title;
                    $blogs->slug = $slug;
                    $blogs->description = $description;
                    $blogs->author = $author;
                    $blogs->tags = $tags;
                    $blogs->keywords = $keywords;
                    $blogs->update();
                    return Redirect::to('admin/blogs')->withInput()->with('alert-success', 'The data saved successfully');
            }
            }





            public function adminNewsEvents(){
                $event = new Event();
                $events = $event->orderBy('created_at', 'DSC')->get();
                return View::make('admin/news-events', compact('events')); 
            }
    
            public function adminNewsEventsAdd(){
                return View::make('admin/add-news-events', compact('')); 
            }
    
            public function adminNewsEventsStore(Request $request){
                $image = request()->input('image');
                $title = request()->input('title');
                $slug = request()->input('slug');
                $story = request()->input('story');
                $author = request()->input('author');
                $topic = request()->input('topic');
                $type = request()->input('type');
                $rules = ([
                    'title'    => 'required',
                    'slug'    => 'required',
                    'story' => 'required',
                    'author' => 'required',
                    'topic' => 'required',
                    'type' => 'required',
                ]);
        
                $validator = Validator::make($request->all(), $rules);
        
                if ($validator->fails()) {
                    return Redirect::back()->withInput()->with('alert-danger', 'The data wasn not saved successfully')->withErrors($validator);
                }
                else{
                        $newsevent = new Event();
                        $newsevent->image = $image;
                        $newsevent->title = $title;
                        $newsevent->slug = $slug;
                        $newsevent->story = $story;
                        $newsevent->author = $author;
                        $newsevent->topic = $topic;
                        $newsevent->type = $type;
                        $newsevent->save();
                        return Redirect::to('admin/news-events')->withInput()->with('alert-success', 'The data saved successfully');
                }
            }
    
            public function adminNewsEventsEdit($id){
                $event = new Event();
                $event = $event->find($id);
                return View::make('admin/edit-news-events', compact('event')); 
            }
    
            public function adminNewsEventsUpdate($id, Request $request){
                $image = request()->input('image');
                $title = request()->input('title');
                $slug = request()->input('slug');
                $story = request()->input('story');
                $author = request()->input('author');
                $topic = request()->input('topic');
                $type = request()->input('type');
                $event = new Event();
                $events = $event->find($id);
                $rules = ([
                    'title'    => 'required',
                    'slug'    => 'required',
                    'story' => 'required',
                    'author' => 'required',
                    'topic' => 'required',
                    'type' => 'required',
                ]);
        
                $validator = Validator::make($request->all(), $rules);
        
                if ($validator->fails()) {
                    return Redirect::back()->withInput()->with('alert-danger', 'The data wasn not saved successfully')->withErrors($validator);
                }
                else{
                        $events->image = $image;
                        $events->title = $title;
                        $events->slug = $slug;
                        $events->story = $story;
                        $events->author = $author;
                        $events->topic = $topic;
                        $events->type = $type;
                        $events->update();
                        return Redirect::to('admin/news-events')->withInput()->with('alert-success', 'The data saved successfully');
                }
                }














        public function adminAnswerKeys(){
            $answerkeys = Answerkey::all();
            return View::make('admin/answer-keys', compact('answerkeys')); 
        }

        public function adminAnswerKeysAdd(){
            return View::make('admin/add-answer-keys'); 
        }

        public function adminAnswerKeysStore(Request $request){
            $answerkeys = request()->input('answerkeys');
            $category = request()->input('category');
            $test = request()->input('test');
            $exam = request()->input('exam');
            $rules = ([
                'answerkeys'    => 'required',
                'category'    => 'required',
                'test' => 'required',
                'exam' => 'required',
            ]);
    
            $validator = Validator::make($request->all(), $rules);
    
            if ($validator->fails()) {
                return Redirect::back()->withInput()->with('alert-danger', 'The data wasn not saved successfully')->withErrors($validator);
            }
            else{
                    $answerkey= new Answerkey();
                    $answerkey->answerkeys = $answerkeys;
                    $answerkey->category = $category;
                    $answerkey->test = $test;
                    $answerkey->exam = $exam;
                    $answerkey->save();
                    return Redirect::to('admin/answer-keys/')->withInput()->with('alert-success', 'The data saved successfully');
            }
        }

        public function adminAnswerKeysEdit($id){
            $answerkey = new Answerkey();
            $answerkey = $answerkey->where('id', $id)->first();
            return View::make('admin/edit-answer-keys', compact('answerkey')); 
        }

        public function adminAnswerKeysUpdate($id, Request $request){
            $answerkeys = request()->input('answerkeys');
            $category = request()->input('category');
            $test = request()->input('test');
            $exam = request()->input('exam');
            $answerkey= new Answerkey();
            $answerkey = $answerkey->find($id);
            $rules = ([
                'answerkeys'    => 'required',
                'category'    => 'required',
                'test' => 'required',
                'exam' => 'required',
            ]);
    
            $validator = Validator::make($request->all(), $rules);
    
            if ($validator->fails()) {
                return Redirect::back()->withInput()->with('alert-danger', 'The data wasn not saved successfully')->withErrors($validator);
            }
            else{
                    $answerkey->answerkeys = $answerkeys;
                    $answerkey->category = $category;
                    $answerkey->test = $test;
                    $answerkey->exam = $exam;
                    $answerkey->update();
                    return Redirect::to('admin/answer-keys/')->withInput()->with('alert-success', 'The data saved successfully');
            }
        }

        public function adminUploadPic(Request $request){
           
            $rules = ([
             'file' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
         ]);
 
         $validator = Validator::make($request->all(), $rules);

            if($validator->passes())
            {
             $image = request()->file('file');
             $new_name = rand() . '.' . $image->getClientOriginalExtension();
               $image->move(public_path('images/admin_uploads'), $new_name);
               return response()->json([
                'message'   => 'Image Upload Successfully',
                'uploaded_image' => '<img src="/images/admin_uploads/'.$new_name.'" class="img-thumbnail" width="300" />',
                'class_name'  => 'alert-success',
                'file_name' => $new_name
               ]);
            }
            else
             {
             return response()->json([
             'message'   => $validator->errors()->all(),
             'uploaded_image' => '',
             'class_name'  => 'alert-danger'
             ]);
             }
     }
        public function updatePostStatus(Request $request){
            $posttype = request()->input('posttype');
            $postid = request()->input('postid');
            $poststatus = request()->input('poststatus');
            $catch_object = "\\App\\".$posttype;
            $new_object = $catch_object::find($postid);
            $new_object->status = $poststatus == '1' ? '0' : '1';
            $new_object->update();
            return response()->json([
                'message'   => 'updated',
                'post_status' => $new_object->status,
                'post_id' => $postid,
                'post_type'  => $posttype
            ]);
        }
    }

