<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use App\Cuecard;
use App\Cuecardmodel;
class CourseController extends Controller
{
    public function listening(){
        return View::make('listening'); 
    }

    public function listeningTips(){
        return View::make('listening-tips'); 
    }

    public function listeningSamples(){
        return View::make('listening-samples'); 
    }

    public function writing(){
        return View::make('writing'); 
    }

    public function writingTips(){
        return View::make('writing-tips'); 
    }

    public function speaking(){
        return View::make('speaking'); 
    }

    public function speakingTips(){
        return View::make('speaking-tips'); 
    }

    public function speakingCueCards(){
        $cuecard = new Cuecard();
        $cuecards = $cuecard->where('status', 1)->paginate(10);
        return View::make('speaking-cue-cards', compact('cuecards')); 
    }

    public function speakingCueCardsDisplay($id){
        $cuecard = new Cuecard();
        $cuecard_find = $cuecard->find($id);
        $cuecard_models = $cuecard_find->cuecardmodels;
        return View::make('speaking-cue-cards-display', compact('cuecard_find','cuecard_models'));
    }

    public function reading(){
        return View::make('reading'); 
    }

    public function readingTips(){
        return View::make('reading-tips'); 
    }

    public function readingExcercises(){
        return View::make('reading-excercises'); 
    }

    public function readingExcercisesDisplay($id){
        $cuecard = new Cuecard();
        $cuecards = $cuecard->with([
            'cuecardmodels' => function ($query) use ($id) {
                $query->where('cuecards_id', $id);
            },
        ])->where('id', $id)->first();
        $from = 4;
        return View::make('reading-excercises-display', compact('cuecards', 'from')); 
    }
    
}
