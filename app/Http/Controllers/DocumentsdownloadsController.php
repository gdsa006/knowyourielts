<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;

class DocumentsdownloadsController extends Controller
{
    public function index(){
        return View::make('documents-download');
    }
}
