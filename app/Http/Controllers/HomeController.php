<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use App\How;
use App\Event;
use App\Blog;

class HomeController extends Controller
{
    public function index(){
        $how = new How();
        $hows = $how->inRandomOrder()->limit(1)->get();

        $event = new Event();
        $events = $event->orderBy('created_at', 'ASC')->get();

        $blog = new Blog();
        $blogs = $blog->orderBy('created_at', 'ASC')->take(2)->get();

        return View::make('home', compact('hows', 'events', 'blogs'));
    }
}
