<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use App\Calculator;

class CalculatorController extends Controller
{
    public function index(){
        return View::make('calculator');
    }

    public function store(){
        // $input = request()->all();
        $listeningBand = request()->input('listeningBand');
        $readingBand = request()->input('readingBand');
        $writingBand = request()->input('writingBand');
        $speakingBand = request()->input('speakingBand');
        
        $final_avg = ($listeningBand + $readingBand + $writingBand + $speakingBand)/4;
        
        $final_score = round($final_avg);
        
        $calculator = new Calculator();
        $calculator->listening = $listeningBand;
        $calculator->reading = $readingBand;
        $calculator->writing = $writingBand;
        $calculator->speaking = $speakingBand;
        $calculator->scores = $final_score;
        $calculator->ip = request()->ip();
        $calculator->save();

        return response()->json(['Data'=>$final_score]);
    }

}
