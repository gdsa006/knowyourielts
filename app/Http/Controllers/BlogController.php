<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use App\Blog;
class BlogController extends Controller
{
    public function index(){
        $blog = new Blog();
        $blogs = $blog->where('status', 1)->orderBy('created_at', 'ASC')->paginate(6);
        return View::make('blog', compact('blogs'));
    }

    public function details($slug){
        $blog = Blog::where('slug', $slug)->first();
        return View::make('blog-view', compact('blog')); //actually variable should be singular here
    }
}
