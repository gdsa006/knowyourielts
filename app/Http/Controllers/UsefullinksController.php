<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;

class UsefullinksController extends Controller
{
    public function index(){
        return View::make('useful-links'); 
    }
}
