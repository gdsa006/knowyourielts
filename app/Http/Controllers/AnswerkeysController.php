<?php

namespace App\Http\Controllers;

use App\Answerkey;
use View;
use Illuminate\Http\Request;

class AnswerkeysController extends Controller
{
    public function index(){
        $answerkey = new Answerkey();
        $answerkeys = $answerkey->where('status', 1)->paginate(6);
        return View::make('answer-keys', compact('answerkeys'));
    }
}
