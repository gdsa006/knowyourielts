<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use Illuminate\Support\Facades\Mail;
use App\Contact;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    public function index(){
        return View::make('contactus'); 
    }

    public function sendMail(Request $request){
		$name = request()->input('name');
		$email = request()->input('email');
        $msg = request()->input('message');
        
        $validator = Validator::make($request->all(), [
            'name'    => 'required',
			'email'    => 'email',
			'message' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
				'message'   => $validator->errors(),
				]);
        }
        else {
			$contact = new Contact;
			$contact->name = request()->input('name');
			$contact->email = request()->input('email');
			$contact->message = request()->input('message');
			$contact->save();
			
			Mail::send('contact-mail', ['msg' => $msg, 'email' => $email, 'name' => $name], function ($message) use($contact){
				$message->subject('Message: '.$contact->message);
				$message->from('contact@knowyourielts.com', 'KYIS');
				$message->replyTo($contact->email, $contact->name);
				$message->cc('rock.gold0076@gmail.com', 'Gian');
				$message->cc('gdsa006@gmail.com', 'Gagan');
				$message->to('knowyourielts@gmail.com') ;
			});

			return response()->json([
				'message'   => 'success',
			]);
		}
	}

}
