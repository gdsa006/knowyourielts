<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use View;
use Illuminate\Http\Request;
use App\Contact;
use App\Hit;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */

    public function boot(Request $request)
    {
        Schema::defaultStringLength(191);
        
        $hit = new Hit();
        $hit->url = $request->url();
        $hit->ipaddress = $_SERVER['REMOTE_ADDR'];
        
        $pathCheck = $request->path();
        if(!str_contains($pathCheck, 'admin/'))
        {
            $hit->save();
        }

        $contact = new Contact();
        $contacts = $contact->orderBy('created_at', 'DSC')->limit(5)->get();

        View::share('contacts', $contacts);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
