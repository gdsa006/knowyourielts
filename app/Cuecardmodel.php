<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Cuecardmodel extends Authenticatable
{
    
	protected $table = 'cuecardmodels';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['model', 'cuecards_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function cuecard()
	{
		return $this->belongsTo(Cuecard::class);
	}
}
